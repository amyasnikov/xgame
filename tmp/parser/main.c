#include "parser.h"
#include "lexer.h"

int parse(const char* ptr) {
  int result;
  yyscan_t scanner;

  yylex_init(&scanner);
  yy_scan_string(ptr, scanner);
  result = (yyparse(scanner));
  yylex_destroy(scanner);
  return result;
}

int main(int argc, char **argv) {

  if (argc < 1)
    return 0;

  printf("argv[1] = \"%s\";\n", argv[1]);
  int err = parse(argv[1]);
  printf("err = %d\n", err);

  return 0;
}
