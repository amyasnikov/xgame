%define parse.error verbose
%define api.pure true
%locations
%token-table
%glr-parser
%lex-param {void *scanner}
%parse-param {void *scanner}

%{
/* your top code here */
class A{};
%}

%union {
  int    ival;
  double dval;
  struct {
    char *ptr;
    int  len;
  } sval;
}

%token <ival> ICONST
%token <dval> FCONST
%token <sval> WORD

%token <sval> NOTHING MOVE

%type <dval> double

%%

stmtblock:
  stmtmulti
  ;

stmtmulti:
  stmtmulti ';' stmt
  | stmt
  ;

stmt:
  NothingStmt
  | MoveStmt
  | %empty
  ;

NothingStmt:
  NOTHING                     { printf("NotingStmt: %.*s\n", $1.len, $1.ptr); }
  ;

MoveStmt:
  MOVE WORD double double     { printf("MoveStmt: (\"%.*s\", \"%.*s\", %f, %f)\n", $1.len, $1.ptr, $2.len, $2.ptr, $3, $4); }
  ;

double:
  FCONST                      { $$ = $1; }
  | ICONST                    { $$ = (double) $1; }
  ;

%%

int
yyerror(YYLTYPE *locp, char *msg) {
  if (locp) {
    fprintf(stderr, "parse error: %s (:%d.%d -> :%d.%d)\n",
                    msg,
                    locp->first_line, locp->first_column,
                    locp->last_line,  locp->last_column
    );
    /* todo: add some fancy ^^^^^ error handling here */
  } else {
    fprintf(stderr, "parse error: %s\n", msg);
  }
  return (0);
}
