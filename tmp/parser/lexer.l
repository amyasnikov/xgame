%option case-sensitive
%option bison-bridge
%option bison-locations
%option ecs
%option nodefault
%option noyywrap
%option reentrant
%option stack
%option warn
%option yylineno

digit               [0-9]
self                [{},:;]
integer             -?{digit}+
double              -?(({digit}*\.{digit}+)|({digit}+\.{digit}*))
ident_start         [A-Za-z]
ident_cont          [A-Za-z0-9]
identifier          {ident_start}{ident_cont}*
other               .

%{
#include "parser.h"
#define RETURN_TOKEN yylval->sval.ptr = yytext; yylval->sval.len = yyleng; return
%}

%%

{integer}           { yylval->ival = atoi(yytext); return ICONST; }
{double}            { yylval->dval = atof(yytext); return FCONST; }
move                { RETURN_TOKEN MOVE; }
nothing             { RETURN_TOKEN NOTHING; }
{identifier}        { RETURN_TOKEN WORD; }
{self}              { return yytext[0]; }
[ \f\r\t\v\n]+      { }

%%

/* don't use lexer.l for code, organize it logically elsewhere */
