
#include <iostream>
#include "json.hpp"

using nlohmann::json;
using namespace std;

int main() {
    json j = {
        {"pi", 3.141},
        {"happy", true},
        {"name", "Niels"},
        {"nothing", nullptr},
        {"answer", {
            {"everything", 42}
        }},
        {"list", {1, 0, 2}},
        {"object", {
            {"currency", "USD"},
            {"value", 42.99}
        }}
    };

    cout << j.dump(2) << endl;
}

