
#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>
#include <errno.h>
#include <cstring>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <assert.h>
#include <chrono>
#include <thread>



using namespace std;



enum WebSocketFrameType {
  ERROR_FRAME=0xFF00,
  INCOMPLETE_FRAME=0xFE00,

  OPENING_FRAME=0x3300,
  CLOSING_FRAME=0x3400,

  INCOMPLETE_TEXT_FRAME=0x01,
  INCOMPLETE_BINARY_FRAME=0x02,

  TEXT_FRAME=0x81,
  BINARY_FRAME=0x82,

  PING_FRAME=0x19,
  PONG_FRAME=0x1A
};



char *base64(const unsigned char *input, int length)
{
  BIO *bmem, *b64;
  BUF_MEM *bptr;

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char *buff = (char *)malloc(bptr->length);
  memcpy(buff, bptr->data, bptr->length-1);
  buff[bptr->length-1] = 0;

  BIO_free_all(b64);

  return buff;
}

std::string createResponse(std::string str) {
  std::string key = "";
  std::string fstr = "Sec-WebSocket-Key: ";

  std::size_t found = str.find(fstr);
  if (found!=std::string::npos) {
    key = str.substr(found + fstr.size(), 24);
  } else {
    return "test;";
  }
  std::cout << "key = [" << key << "]" << std::endl;

  key += "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
  std::cout << "key = [" << key << "]" << std::endl;

  std::string hash(20, '\0');
  const unsigned char *p;
  SHA1((unsigned char *) key.c_str(), key.size(), (unsigned char *) hash.c_str());
  std::cout << "key = [" << hash << "]" << std::endl;

  std::cout << "key = [" << key << "]" << std::endl;

  char *output = base64((const unsigned char *) hash.c_str(), hash.size());
  key = output;
  std::cout << "key = [" << key << "]" << std::endl;



  std::string result =
    "HTTP/1.1 101 Switching Protocols\r\n"
    "Upgrade: websocket\r\n"
    "Connection: Upgrade\r\n"
    "Sec-WebSocket-Accept: " + key + "\r\n"
    "Sec-WebSocket-Protocol: chat superchat\r\n\r\n";
  return result;
}

int makeFrame(WebSocketFrameType frame_type, unsigned char *msg, int msg_length, unsigned char *buffer, int buffer_size) {

  int pos = 0;
  int size = msg_length;
  buffer[pos++] = (unsigned char)frame_type; // text frame

  if(size <= 125) {
    buffer[pos++] = size;
  }
  else if(size <= 65535) {
    buffer[pos++] = 126; //16 bit length follows

    buffer[pos++] = (size >> 8) & 0xFF; // leftmost first
    buffer[pos++] = size & 0xFF;
  }
  else { // >2^16-1 (65535)
    buffer[pos++] = 127; //64 bit length follows

    // write 8 bytes length (significant first)

    // since msg_length is int it can be no longer than 4 bytes = 2^32-1
    // padd zeroes for the first 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = 0;
    }
    // write the actual 32bit msg_length in the next 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = ((size >> 8*i) & 0xFF);
    }
  }
  memcpy((void*)(buffer+pos), msg, size);
  return (size+pos);
}


int getFrame(unsigned char *buf_in, int len_in, std::string &buf_out) {
  // original: https://github.com/katzarsky/WebSocket/blob/master/WebSocket/WebSocket.cpp

  std::cout << "begin getFrame()" << std::hex << std::endl;

  unsigned char msg_opcode =  buf_in[0] & 0x0F;
  unsigned char msg_fin =    (buf_in[0] >> 7) & 0x01;
  unsigned char msg_masked = (buf_in[1] >> 7) & 0x01;

  std::cout << "msg flags : " << (int) msg_opcode << "  " << (int) msg_fin << "  " << (int) msg_masked << std::endl;


  int payload_length = 0;
  int pos = 2;
  int length_field = buf_in[1] & (~0x80);
  unsigned int mask = 0;

  std::cout << "length_field : " << length_field << std::endl;


  if(length_field <= 125) {
    payload_length = length_field;
  }
  else if(length_field == 126) { //msglen is 16bit!
    payload_length = buf_in[2] + (buf_in[3]<<8);
    pos += 2;
  }
  else if(length_field == 127) { //msglen is 64bit!
    payload_length = buf_in[2] + (buf_in[3]<<8);
    pos += 8;
  }

  std::cout << "payload_length : " << payload_length << std::endl;


  if(len_in < payload_length + pos) {
    return 0;
  }

  if(msg_masked) {
    mask = *((unsigned int*)(buf_in + pos));
    pos += 4;

    // unmask data:
    unsigned char* c = buf_in + pos;
    for(int i = 0; i < payload_length; i++) {
      c[i] = c[i] ^ ((unsigned char*)(&mask))[i % 4];
      std::cout << "c[i] : " << (int) c[i] << std::endl;
    }
  }

  std::cout << "mask : " << mask << std::endl;





  std::cout << "end getFrame()" << std::dec << std::endl << std::endl;
  return 0;
}


int main( ) {

  std::cout << "Server.cpp" << endl;


  std::string request =
    "buf = GET / HTTP/1.1\n"
    "Host: 192.168.1.8:3456\n"
    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0\n"
    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n"
    "Accept-Language: en-US,en;q=0.5\n"
    "Accept-Encoding: gzip, deflate\n"
    "Sec-WebSocket-Version: 13\n"
    "Origin: null\n"
    "Sec-WebSocket-Extensions: permessage-deflate\n"
    "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==\n"
    "Connection: keep-alive, Upgrade\n"
    "Pragma: no-cache\n"
    "Cache-Control: no-cache\n"
    "Upgrade: websocket\n"
    "\n";

  // cout << request << endl;
  // cout << createResponse(request) << endl;



  const int n = 1000;
  char buf[n];
  memset(buf, 0, n);

  int s = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in sa;
  memset(&sa, 0, sizeof sa);
  sa.sin_family = AF_INET;
  sa.sin_port = htons(3456);
  if (bind(s, (struct sockaddr *)&sa, sizeof sa) < 0) {
    std::cout << "bind(..) < 0 " << std::endl;
    return 0;
  }

  listen(s, 10);

  while (true) {
    std::cout << "true" << endl;
    int ConnectFD = accept(s, NULL, NULL);
    std::cout << "true2" << endl;

    // std::cout << ConnectFD << endl;
    ssize_t len = recv(ConnectFD, buf, n, MSG_NOSIGNAL);
    std::cout << "len = " << len << std::endl;
    // std::cout << "err = " << errno << std::endl;
    std::cout << "recv = [" << buf << "]" << std::endl;
    // for (int i = 0; i < len; i++) {
    //   std::cout << "buf[" << i << "] = " << buf[i] << "  \t" << buf[i] << std::endl;
    // }


    std::string response = createResponse(buf);
    std::cout << "response = [" << response << "]" << std::endl;
    // if (response == "test;")
    //   continue;
    ssize_t k = send(ConnectFD, response.c_str(), response.size(), MSG_NOSIGNAL);
    std::cout << "k send = " << k << std::endl;

    int t = 0;
    while (true)
    {
      std::cout << "t = " << t++ << std::endl;
      memset(buf, 0, n);
      ssize_t len = recv(ConnectFD, buf, n, MSG_NOSIGNAL);
      std::cout << "len = " << len << std::endl;
      // std::cout << "err = " << errno << std::endl;
      std::cout << "recv2 = [" << buf << "]" << std::endl;
      for (int i = 0; i < len; i++) {
        std::cout << "buf[" << i << "] = " << std::hex << (int) buf[i] << "  \t" << std::dec << buf[i] << std::endl;
      }

      std::string buf_in(buf, len), buf_out;
      getFrame((unsigned char*)buf, len, buf_out);


      if (len > 1) {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        len = makeFrame(TEXT_FRAME, (unsigned char *) "OLOLO123", 8, (unsigned char *) buf, n);
        for (int i = 0; i < len; i++) {
          std::cout << "buf_[" << i << "] = " << std::hex << (int) buf[i] << "  \t" << std::dec << buf[i] << std::endl;
        }
        ssize_t k = send(ConnectFD, buf, len, MSG_NOSIGNAL);
        std::cout << "k = " << k << std::endl;
      }




      if (t > 20)
        break;
    }

    // {
    //   ssize_t len = recv(ConnectFD, buf, n, MSG_NOSIGNAL);
    //   std::cout << "len = " << len << std::endl;
    //   // std::cout << "err = " << errno << std::endl;
    //   std::cout << "buf = " << buf << std::endl;
    //   for (int i = 0; i < len; i++) {
    //     std::cout << "buf[" << i << "] = " << buf[i] << "  \t" << buf[i] << std::endl;
    //   }
    // }

    // {
    //   std::string response("\x00_ server hi;_\xFF", 15);
    //   for (int i = 0; i < response.size(); i++)
    //     std::cout << "response[i] = " << response[i] << "  \t" << (int) response[i] << std::endl;

    //   std::cout << "response = " << response << std::endl;
    //   ssize_t k = send(ConnectFD, response.c_str(), response.size(), MSG_NOSIGNAL);
    //   std::cout << "k = " << k << std::endl;
    // }
  }




  std::cout << "end..." << endl;

  return 0;
}
