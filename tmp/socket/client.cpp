
#include <iostream>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <cstring>



using namespace std;

int main( ) {

  std::cout << "Client.cpp" << endl;

  const int n = 1000;
  char buf[n];
  memset(buf, 0, n);

  int s = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in sa;
  memset(&sa, 0, sizeof sa);
  sa.sin_family = AF_INET;
  sa.sin_port = htons(3456);
  int res = inet_pton(AF_INET, "192.168.1.8", &sa.sin_addr);
  std::cout << "res = " << res << endl;

  if (-1 == connect(s, (struct sockaddr *)&sa, sizeof sa)) {
    std::cout << "connect == -1" << endl;
    // close(s);
  }

  std::string request =
    "buf = GET / HTTP/1.1\n"
    "Host: 192.168.1.8:3456\n"
    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0\n"
    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n"
    "Accept-Language: en-US,en;q=0.5\n"
    "Accept-Encoding: gzip, deflate\n"
    "Sec-WebSocket-Version: 13\n"
    "Origin: null\n"
    "Sec-WebSocket-Extensions: permessage-deflate\n"
    "Sec-WebSocket-Key: jdGhlIHNhbXBsZSBub25jZQ==\n"
    "Connection: keep-alive, Upgrade\n"
    "Pragma: no-cache\n"
    "Cache-Control: no-cache\n"
    "Upgrade: websocket\n"
    "\n";

  // memcpy(buf, "Hello from client!!!\0", 25);
  ssize_t k = send(s, request.c_str(), request.size(), MSG_NOSIGNAL);
  memcpy(buf, "client.cpp", 10);
  send(s, buf, 10, MSG_NOSIGNAL);
  send(s, buf, 10, MSG_NOSIGNAL);
  std::cout << "k = " << k << endl;

  ssize_t len = recv(s, buf, n, MSG_NOSIGNAL);
  std::cout << "len = " << len << std::endl;
  std::cout << "buf = " << buf << std::endl;



  std::cout << "end..." << endl;

  return 0;
}
