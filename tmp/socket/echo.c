
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <ev.h>

#define MSGN 1000
#define FDN 100



int fds[FDN];

struct msg {
  char str[1024];
  int len;
};

struct msg msgs[MSGN];



enum WebSocketFrameType {
  ERROR_FRAME=0xFF00,
  INCOMPLETE_FRAME=0xFE00,

  OPENING_FRAME=0x3300,
  CLOSING_FRAME=0x3400,

  INCOMPLETE_TEXT_FRAME=0x01,
  INCOMPLETE_BINARY_FRAME=0x02,

  TEXT_FRAME=0x81,
  BINARY_FRAME=0x82,

  PING_FRAME=0x19,
  PONG_FRAME=0x1A
};



char *base64(const unsigned char *input, int length)
{
  BIO *bmem, *b64;
  BUF_MEM *bptr;

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char *buff = (char *)malloc(bptr->length);
  memcpy(buff, bptr->data, bptr->length-1);
  buff[bptr->length-1] = 0;

  BIO_free_all(b64);

  return buff;
}

int createResponse(const unsigned char* request, int req_len, unsigned char* response, int resp_max_len) {
  printf("request: [%.*s]\n", req_len, request);

  if (req_len < 14 || strncmp((const char *) request, (const char*) "GET / HTTP/1.1", 14) != 0)
    return 0;

  char *key = strstr(request, "Sec-WebSocket-Key: ");
  printf("key: %p\n", key);
  if (key == 0)
    return 0;

  key += 19;
  printf("key: [%.*s]\n", 24, key);

  char key_ext[24 + 36];
  memcpy(key_ext, key, 24);
  memcpy(key_ext + 24, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", 36);
  printf("key_ext: [%.*s]\n", 24 + 36, key_ext);

  char hash[20];
  SHA1((unsigned char *) key_ext, 24 + 36, (unsigned char *) hash);
  printf("hash: [%.*s]\n", 20, hash);

  char *output = base64((const unsigned char *) hash, 20);
  printf("output: [%s]\n", output);

  char result[] = {
    "HTTP/1.1 101 Switching Protocols\r\n"
    "Upgrade: websocket\r\n"
    "Connection: Upgrade\r\n"
    "Sec-WebSocket-Accept: ____________________        \r\n\r\n"
    /* "Sec-WebSocket-Protocol: chat superchat\r\n\r\n" */
  };

  memcpy(strstr(result, "_____"), output, strlen(output));

  int len = strlen(result);
  memcpy(response, result, len);

  printf("result: [%.*s]\n", len, result);

  return len;
}

int makeFrame(enum WebSocketFrameType frame_type, unsigned char *msg, int msg_length, unsigned char *buffer, int buffer_size) {

  int pos = 0;
  int size = msg_length;
  buffer[pos++] = (unsigned char)frame_type; // text frame

  if(size <= 125) {
    buffer[pos++] = size;
  }
  else if(size <= 65535) {
    buffer[pos++] = 126; //16 bit length follows

    buffer[pos++] = (size >> 8) & 0xFF; // leftmost first
    buffer[pos++] = size & 0xFF;
  }
  else { // >2^16-1 (65535)
    buffer[pos++] = 127; //64 bit length follows

    // write 8 bytes length (significant first)

    // since msg_length is int it can be no longer than 4 bytes = 2^32-1
    // padd zeroes for the first 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = 0;
    }
    // write the actual 32bit msg_length in the next 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = ((size >> 8*i) & 0xFF);
    }
  }
  memcpy((void*)(buffer+pos), msg, size);
  return (size+pos);
}

enum WebSocketFrameType getFrame(unsigned char* in_buffer, int in_length, unsigned char* out_buffer, int out_size, int* out_length)
{
  //printf("getTextFrame()\n");
  if(in_length < 3)
    return INCOMPLETE_FRAME;

  unsigned char msg_opcode =  in_buffer[0] & 0x0F;
  unsigned char msg_fin    = (in_buffer[0] >> 7) & 0x01;
  unsigned char msg_masked = (in_buffer[1] >> 7) & 0x01;

  // *** message decoding

  int payload_length = 0;
  int pos = 2;
  int length_field = in_buffer[1] & (~0x80);
  unsigned int mask = 0;

  //printf("IN:"); for(int i=0; i<20; i++) printf("%02x ",buffer[i]); printf("\n");

  if(length_field <= 125) {
    payload_length = length_field;
  }
  else if(length_field == 126) { //msglen is 16bit!
    payload_length = in_buffer[2] + (in_buffer[3]<<8);
    pos += 2;
  }
  else if(length_field == 127) { //msglen is 64bit!
    payload_length = in_buffer[2] + (in_buffer[3]<<8);
    pos += 8;
  }
  //printf("PAYLOAD_LEN: %08x\n", payload_length);
  if(in_length < payload_length+pos) {
    return INCOMPLETE_FRAME;
  }

  if(msg_masked) {
    mask = *((unsigned int*)(in_buffer+pos));
    //printf("MASK: %08x\n", mask);
    pos += 4;

    // unmask data:
    unsigned char* c = in_buffer+pos;
    for(int i=0; i<payload_length; i++) {
      c[i] = c[i] ^ ((unsigned char*)(&mask))[i%4];
    }
  }

  if(payload_length > out_size) {
    //TODO: if output buffer is too small -- ERROR or resize(free and allocate bigger one) the buffer ?
  }

  memcpy((void*)out_buffer, (void*)(in_buffer+pos), payload_length);
  /* out_buffer[payload_length] = 0; */
  /* *out_length = payload_length+1; */
  *out_length = payload_length;

  //printf("TEXT: %s\n", out_buffer);

  if (msg_opcode == 0x0) return (msg_fin)?TEXT_FRAME:INCOMPLETE_TEXT_FRAME; // continuation frame ?
  if (msg_opcode == 0x1) return (msg_fin)?TEXT_FRAME:INCOMPLETE_TEXT_FRAME;
  if (msg_opcode == 0x2) return (msg_fin)?BINARY_FRAME:INCOMPLETE_BINARY_FRAME;
  if (msg_opcode == 0x8) return CLOSING_FRAME;
  if (msg_opcode == 0x9) return PING_FRAME;
  if (msg_opcode == 0xA) return PONG_FRAME;

  return ERROR_FRAME;
}

void read_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
  char buffer[1024];
  ssize_t r = recv(watcher->fd, buffer, 1024, MSG_NOSIGNAL);
  if (r < 0)
    return;
  else if (r == 0) {
    ev_io_stop(loop, watcher);
    free(watcher);
    for (int i = 0; i < FDN; i++) {
      if (fds[i] == watcher->fd) {
        fds[i] = 0;
        break;
      }
    }
    return;
  } else {
    printf("r: %d\n", r);
    printf("recv: [%.*s]\n", r, buffer);
    char buffer2[1024];
    int len = createResponse(buffer, r, buffer2, 1024);
    printf("len: %d\n", len);

    if (len == 0) {
      enum WebSocketFrameType type = getFrame(buffer, r, buffer2, 1024, &len);
      printf("type: 0x%x\n", (int) type);
      printf("frame: len = %d, buf = [%.*s]\n", len, len, buffer2);

      if (type == CLOSING_FRAME) {
        ev_io_stop(loop, watcher);
        free(watcher);
        for (int i = 0; i < FDN; i++) {
          if (fds[i] == watcher->fd) {
            fds[i] = 0;
            break;
          }
        }
        return;
      }

      for (int i = 0; i < MSGN; i++) {
        if (msgs[i].len == 0) {
          memcpy(msgs[i].str, buffer2, len);
          msgs[i].len = len;
          for (int k = 0; k < msgs[i].len; k++) {
            printf("msgs_[i]: %d\n", (int) msgs[i].str[k]);
          }
          break;
        }
      }
    } else {
      printf("send: [%.*s]\n", len, buffer2);
      send(watcher->fd, buffer2, len, MSG_NOSIGNAL);
    }
  }
}

void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
  int client_sd = accept(watcher->fd, 0, 0);

  struct ev_io *w_client = (struct ev_io *) malloc(sizeof(struct ev_io));
  ev_io_init(w_client, read_cb, client_sd, EV_READ);
  ev_io_start(loop, w_client);

  for (int i = 0; i < FDN; i++) {
    if (fds[i] == 0) {
      fds[i] = client_sd;
      break;
    }
  }
}

void idle_cb(struct ev_loop *loop, struct ev_idle *watcher, int revents) {
  for (int i = 0; i < MSGN; i++) {
    if (msgs[i].len != 0) {
      for (int j = 0; j < FDN; j++) {
        if (fds[j] != 0) {
          printf("send: fd = %d, len = %d, msg = [%.*s]\n", fds[j], msgs[i].len, msgs[i].len, msgs[i].str);

          for (int k = 0; k < msgs[i].len; k++) {
            printf("msgs[i]: %d\n", (int) msgs[i].str[k]);
          }

          char buf[1024];
          int len = makeFrame(TEXT_FRAME, (unsigned char *) msgs[i].str, msgs[i].len, (unsigned char *) buf, 1024);

          int r = send(fds[j], buf, len, MSG_NOSIGNAL);
          printf("send: r = %d\n", r);
        }
      }
      msgs[i].len = 0;
    }
  }
}

int main( ) {

  for (int i = 0; i < FDN; i++) {
    fds[i] = 0;
  }

  for (int i = 0; i < MSGN; i++) {
    msgs[i].len = 0;
  }



  struct ev_loop *loop = ev_default_loop(0);

  int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  printf("sd: %d\n", sd);

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(2468);
  addr.sin_addr.s_addr = htons(INADDR_ANY);
  int b = bind(sd, (struct sockaddr *) &addr, sizeof(addr));
  printf("bind(): %d\n", b);
  if (b < 0)
    return 0;

  int l = listen(sd, SOMAXCONN);
  printf("listen(): %d\n", l);

  struct ev_io w_accept;
  ev_io_init(&w_accept, accept_cb, sd, EV_READ);
  ev_io_start(loop, &w_accept);

  struct ev_idle w_idle;
  ev_idle_init(&w_idle, idle_cb);
  ev_idle_start(loop, &w_idle);



  while (1)
    ev_loop(loop, 0);


  printf("end...\n");
  return 0;
}
