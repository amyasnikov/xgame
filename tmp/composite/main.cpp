
#include <iostream>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <queue>
#include <utility>
#include <cmath>
#include <stdexcept>
#include <random>
#include <algorithm>
#include <thread>
#include <chrono>
#include <mutex>
#include <sstream>
#include <atomic>
#include <experimental/any>
#include "../json/json.hpp"

#include <boost/bind.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>



using namespace std;
using namespace std::experimental;
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>



class Log {
public:
    Log(const char* name, int line) {
        indent += 2;
        fprintf(stderr, "%d %s  %*s  %d: %s\n", indent / 2, "", indent.load(), "", line, name);
    }
    static void log(const char* name, int line, const string& msg) {
        fprintf(stderr, "%d %s  %*s  %d: %s   %s \n", indent / 2, "", indent.load(), "", line, name, msg.c_str());
    }
    ~Log( ) {
        // fprintf(stderr, "%d %s  %*s  %d: ~\n", indent / 2, "", indent, "", line);
        indent -= 2;
    }

    static atomic<int> indent;
};

atomic<int> Log::indent = atomic<int>(0);



#define PR Log log(__PRETTY_FUNCTION__, __LINE__);
#define PF __PRETTY_FUNCTION__
#define PM(msg) Log::log(__FUNCTION__, __LINE__, msg);





namespace composite4 {

    // TODO
    // Убрать Properties.
    // Group: {list<Updater>, list<UpdaterAction>}
    // Sqlite
    // server stop
    // Object: UID -> UID + type
    //

    struct Game;
    struct Pipe;
    struct Action;
    struct Object;
    struct Updater;
    struct UpdaterAction;
    struct UpdaterEvent;

    using ActionSPtr          = shared_ptr<Action>;
    using ObjectSPtr          = shared_ptr<Object>;
    using UpdaterSPtr         = shared_ptr<Updater>;
    using UpdaterActionSPtr   = shared_ptr<const UpdaterAction>;
    using UpdaterEventSPtr    = shared_ptr<const UpdaterEvent>;

    class RandomUniform {
    public:
        RandomUniform( ) { }

        template <class T>
        typename std::enable_if<std::is_integral<T>::value, T>::type
        static next(T a, T b) {
            uniform_int_distribution<T> distribution(0, b - a);
            return distribution(getGenerator()) + a;
        }

        template <class T>
        typename std::enable_if<std::is_floating_point<T>::value, T>::type
        static next(T a, T b) {
            uniform_real_distribution<T> distribution(0, b - a);
            return distribution(getGenerator()) + a;
        }

        template <class T>
        typename std::enable_if<std::is_same<T, std::string>::value, T>::type
        static next(size_t a, size_t b = 0) {
            size_t len = (b == 0) ? a : next(a, b);
            string res;
            res.reserve(len);
            static string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (size_t i = 0; i < len; ++i) {
                int r = next(size_t{0}, chars.size() - 1);
                res.push_back(chars[r]);
            }
            return res;
        }

    private:
        static default_random_engine& getGenerator( ) {
            static default_random_engine generator(time(0));
            return generator;
        }

    };

    enum class StatusUpdate {
        Updated,
        ToUpdate,
        Updating,
    };

    class Listener;

    struct SocketInfo {
        string address;
        unsigned short port;
        size_t thread_count;
        shared_ptr<boost::asio::io_service> ios;
        shared_ptr<Listener> listener;
        vector<thread> threads;
    };

    struct WorldInfo {
        size_t time;
        size_t dt;
        size_t xMin;
        size_t xMax;
        size_t yMin;
        size_t yMax;
    };

    struct LocationInfo {
        pair<size_t, size_t> position;
        size_t direction; // angle
        pair<size_t, size_t> speed;
        size_t size;
    };

    struct ObjectInfo {
        size_t uid;
        size_t parent; // TODO
    };

    struct ListenerInfo {
        list<ActionSPtr> actionsIn;
        mutex mtxIn;
    };

    struct SessionInfo {
        list<ActionSPtr> actionsIn; // Пишет Session под mxtIn.lock(), читает game под mtxIn.try().
        mutex mtxIn;
    };

    enum class PropertyKey {
        Unknown,
        SocketInfo,
        WorldInfo,
        LocationInfo,
        ObjectInfo,
        SessionInfo,
        ListenerInfo,
    };

    template <PropertyKey type> struct PropertyType;
    template <> struct PropertyType<PropertyKey::SocketInfo>           { using type = shared_ptr<SocketInfo>;    };
    template <> struct PropertyType<PropertyKey::WorldInfo>            { using type = shared_ptr<WorldInfo>;     };
    template <> struct PropertyType<PropertyKey::LocationInfo>         { using type = shared_ptr<LocationInfo>;  };
    template <> struct PropertyType<PropertyKey::ObjectInfo>           { using type = shared_ptr<ObjectInfo>;    };
    template <> struct PropertyType<PropertyKey::SessionInfo>          { using type = shared_ptr<SessionInfo>;   };
    template <> struct PropertyType<PropertyKey::ListenerInfo>         { using type = shared_ptr<ListenerInfo>;  };

    struct Properties {
        template <PropertyKey key, class Type = typename PropertyType<key>::type>
        void set( ) {
            properties[key] = Type();
        }

        template <PropertyKey key, class TOut = typename PropertyType<key>::type, class TIn>
        void set(const TIn& value) {
            properties[key] = static_cast<TOut>(value);
        }

        template <PropertyKey key, class Type = typename PropertyType<key>::type>
        void setIfNotExists( ) {
            if (count<key>() == 0)
                properties[key] = Type();
        }

        template <PropertyKey key, class TOut = typename PropertyType<key>::type, class TIn>
        void setIfNotExists(const TIn& value) {
            if (count<key>() == 0)
                properties[key] = static_cast<TOut>(value);
        }

        template <PropertyKey key, class Type = typename PropertyType<key>::type>
        const Type& get( ) const {
            const auto it = properties.find(key);
            if (it != properties.end()) {
                return *any_cast<Type>(&it->second);
            } else {
                throw out_of_range("get( ) : key not found : ");
            }
        }

        template <PropertyKey key, class Type = typename PropertyType<key>::type>
        Type& get( ) {
            return const_cast<Type&>(static_cast<const Properties&>(*this).get<key>());
        }

        template <PropertyKey key>
        void erase( ) {
            properties.erase(key);
        }

        template <PropertyKey key>
        size_t count( ) {
            return properties.count(key);
        }

        using PropertiesType = unordered_map<PropertyKey, any>;

        PropertiesType properties;
    };



    struct UID {
        static constexpr size_t Root                        = 0;
        static constexpr size_t Game                        = 1;
        static constexpr size_t World                       = 2;
        static constexpr size_t GameMng                     = 3;
        static constexpr size_t Socket                      = 4;
        static constexpr size_t Listener                    = 5;
     // static constexpr size_t Session                     = 6;
        static constexpr size_t Last                        = 999;
    };



    struct JN { // JsonName
        static constexpr const char* type                   = "type";
        static constexpr const char* uidSrc                 = "uidSrc";
        static constexpr const char* uidDst                 = "uidDst";
        static constexpr const char* uid                    = "uid";
        static constexpr const char* dx                     = "dx";
        static constexpr const char* dy                     = "dy";
        static constexpr const char* message                = "message";
    };



    struct ActionTypeN {
        static constexpr const string_view Unknown                  = "ActionUnknown"sv;
        static constexpr const string_view Any                      = "Any"sv;
        static constexpr const string_view AddObject                = "AddObject"sv;
        static constexpr const string_view Message                  = "Message"sv;

        static constexpr const string_view EMessage                 = "EventMessage"sv;
    };

    using ActionType = string_view;



    struct Action {
        Action(size_t uidSrc, size_t uidDst, ActionType type)
            : type(type), uidSrc(uidSrc), uidDst(uidDst) { PR; }

        virtual ~Action( ) { PR; }

        friend std::ostream& operator << (std::ostream& stream, const ActionSPtr& action);
        friend std::istream& operator >> (std::istream& stream, ActionSPtr& action);

        const ActionType type;
        const size_t uidSrc;
        const size_t uidDst;
    };



    struct PipeAction {
        PipeAction(ActionType type) : type(type) { PR; }
        virtual ~PipeAction( ) { PR; }
        virtual void load(const nlohmann::json&, ActionSPtr&) = 0;
        virtual void save(nlohmann::json&, const ActionSPtr&) = 0;
        const ActionType type;
    };

    struct Pipe {
        static void load(std::istream&, ActionSPtr&);
        static void save(std::ostream&, const ActionSPtr&);

        static unordered_map<ActionType, shared_ptr<PipeAction>> pipeActions;
    };



    enum class UpdaterActionType {
        Unknown,
        AddObject,

        EMessageGlobal,
    };

    enum class Status {
        Failure,
        Success,
        Neutral,
    };

    struct StatusList {

        void append(const StatusList& status) {
            statusSuccessList.insert(statusSuccessList.end(),
                                     status.statusSuccessList.begin(),
                                     status.statusSuccessList.end());
            statusFailureList.insert(statusFailureList.end(),
                                     status.statusFailureList.begin(),
                                     status.statusFailureList.end());
        }

        struct Args {
            UpdaterActionSPtr updaterAction;
            ObjectSPtr objectSrc;
            ObjectSPtr objectDst;
            ActionSPtr action;
        };

        list<Args> statusSuccessList;
        list<Args> statusFailureList;
    };

    struct UpdaterAction : enable_shared_from_this<UpdaterAction> {
        UpdaterAction(UpdaterActionType type, vector<ActionType> actionTypes)
            : type(type), actionTypes(actionTypes) { }

        virtual StatusList canUpdateAction(const ObjectSPtr&, const ObjectSPtr&,
                                           const Game&, const ActionSPtr&) const = 0;
        virtual void updateActionSuccess(ObjectSPtr&, ObjectSPtr&, Game&, const ActionSPtr&) const = 0;
        virtual void updateActionFailure(ObjectSPtr&, ObjectSPtr&, Game&, const ActionSPtr&) const = 0;

        const UpdaterActionType type;
        const vector<ActionType> actionTypes;
    };

    struct UpdaterEvent : enable_shared_from_this<UpdaterEvent> {
        UpdaterEvent(UpdaterActionType type, vector<ActionType> actionTypes) // TODO
            : type(type), actionTypes(actionTypes) { }

        virtual void updateEvent(ObjectSPtr&, Game&, const ActionSPtr&) const = 0;

        const UpdaterActionType type;
        const vector<ActionType> actionTypes;
    };



    enum class UpdaterType {
        Unknown,
        Socket,
        SocketInit,
        Listener,
        SessionInit,
        Player,
    };

    struct Updater {
        Updater(UpdaterType type) : type(type) { }

        virtual void update(ObjectSPtr&, Game&) const = 0;

        const UpdaterType type;
    };



    struct UpdaterGroup {
    };



    struct Object {
        virtual ~Object() { }

        // TODO UpdaterAction обновляет структуру.
        // По умолчанию добавляются с ключем ActionType::Unknown.
        // unordered_multimap<ActionType, UpdaterActionType> updaterActionTypes;

        // checkEventTrigger(const vector<Trigger>& triggers);

        unordered_set<UpdaterActionType> updaterActionTypes;
        unordered_set<UpdaterType> updaterTypes; // TODO priority
        unordered_multimap<ActionType, UpdaterActionType> updaterEventTypes;
        Properties properties;
    };

    struct ActionUnknown : public Action {
        ActionUnknown( ) : Action(0, 0, ActionTypeN::Unknown) { }
    };

    struct ActionMessage : public Action {
        ActionMessage(size_t uidSrc, size_t uidDst, const string& message)
            : Action(uidSrc, uidDst, ActionTypeN::Message), message(message) {
        }

        string message;
    };

    struct ActionAddObject : public Action {
        ActionAddObject(size_t uidSrc, size_t uidDst, ObjectSPtr object)
            : Action(uidSrc, uidDst, ActionTypeN::AddObject), object(object) {
            PR;
        }

        ObjectSPtr object;
    };

    using Event = Action; // TODO

    struct EventMessage : public Event {
        EventMessage(size_t uidSrc, size_t uidDst, const string& message)
            : Action(uidSrc, uidDst, ActionTypeN::EMessage), message(message) {
        }

        string message;
    };



    struct PipeActionUnknown : PipeAction {
        PipeActionUnknown( ) : PipeAction(ActionTypeN::Unknown) { }

        void load(const nlohmann::json&, ActionSPtr& action) override {
            action = make_shared<ActionUnknown>();
        }

        void save(nlohmann::json& json, const ActionSPtr& action) override {
            json[JN::type] = string(action->type);
        }
    };

    struct PipeMessage : PipeAction {
        PipeMessage( ) : PipeAction(ActionTypeN::Message) { }

        void load(const nlohmann::json& json, ActionSPtr& action) override {
            size_t uidSrc  = json.at(JN::uidSrc);
            size_t uidDst  = json.at(JN::uidDst);
            string message = json.at(JN::message);
            action = make_shared<ActionMessage>(uidSrc, uidDst, message);
        }

        void save(nlohmann::json& json, const ActionSPtr& action) override {
            auto actionMessage = static_pointer_cast<ActionMessage>(action);
            json[JN::type]     = string(actionMessage->type);
            json[JN::uidSrc]   = actionMessage->uidSrc;
            json[JN::uidDst]   = actionMessage->uidDst;
            json[JN::message]  = actionMessage->message;
        }
    };

    struct PipeEventMessage : PipeAction {
        PipeEventMessage( ) : PipeAction(ActionTypeN::EMessage) { }

        void load(const nlohmann::json& json, ActionSPtr& event) override {
            size_t uidSrc  = json.at(JN::uidSrc);
            size_t uidDst  = json.at(JN::uidDst);
            string message = json.at(JN::message);
            event = make_shared<EventMessage>(uidSrc, uidDst, message);
        }

        void save(nlohmann::json& json, const ActionSPtr& event) override {
            auto eventMessage = static_pointer_cast<EventMessage>(event);
            json[JN::type]    = string(eventMessage->type);
            json[JN::uidSrc]  = eventMessage->uidSrc;
            json[JN::uidDst]  = eventMessage->uidDst;
            json[JN::message] = eventMessage->message;
        }
    };



    unordered_map<ActionType, shared_ptr<PipeAction>> Pipe::pipeActions = {
        { ActionTypeN::Unknown,             make_shared<PipeActionUnknown>() },
        { ActionTypeN::Message,             make_shared<PipeMessage>() },
        { ActionTypeN::EMessage,            make_shared<PipeEventMessage>() },
    };



    struct World : public Object {
        World( ) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = UID::World;
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            properties.set<PropertyKey::WorldInfo>(
                make_shared<WorldInfo>(WorldInfo{0, 0, 0, 0, 100, 100}));

            updaterActionTypes = { UpdaterActionType::AddObject };
        }
    };

    struct Player : public Object {
        Player(const pair<size_t, size_t>& position) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = RandomUniform::next((size_t)UID::Last, size_t(-1));
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            properties.set<PropertyKey::LocationInfo>(
                make_shared<LocationInfo>(LocationInfo{position, 0, {0, 0}, 1}));

            // properties.set<PropertyKey::Login>(); // TODO
            updaterActionTypes = { };
            updaterTypes = { UpdaterType::Player };
        }
    };

    struct Gem : public Object {
        Gem(const pair<size_t, size_t>& position) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = RandomUniform::next((size_t)UID::Last, size_t(-1));
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            properties.set<PropertyKey::LocationInfo>(
                make_shared<LocationInfo>(LocationInfo{position, 0, {0, 0}, 1}));
        }
        Gem(const Gem&) = default;
    };

    // struct Bot : public Object {
    //     Bot(const pair<size_t, size_t>& position) {
    //         auto objectInfo = make_shared<ObjectInfo>();
    //         objectInfo->uid = RandomUniform::next((size_t)UID::Last, size_t(-1));
    //         properties.set<PropertyKey::ObjectInfo>(objectInfo);

    //         properties.set<PropertyKey::LocationInfo>(
    //             make_shared<LocationInfo>(LocationInfo{position, 0, {0, 0}, 1}));
    //     }
    // };

    // struct Bomb : public Object {
    //     Bomb(const pair<size_t, size_t>& position) {
    //         properties.set<PropertyKey::LocationInfo>(
    //             make_shared<LocationInfo>(LocationInfo{position, {0, 0}, {0, 0}, 1}));
    //     }
    // };

    struct GemFactory : public Object {
        GemFactory( ) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = RandomUniform::next((size_t)UID::Last, size_t(-1));
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            // updaterActionTypes = { };
            // updaterTypes = { UpdaterType::AddGem };
        }
    };

    struct GameMng : public Object {
        GameMng( ) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = UID::GameMng;
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            updaterActionTypes = { };
            updaterTypes = { };
        }
    };

    template <class Key>
    struct Tree {
        struct Node {
            Key parent;
            unordered_set<Key> children;
        };

        void add(const Key& key, const Key& parent) {
            tree[key] = {parent, {}};
            tree[getParent(key)].children.insert(key); // Чтобы кинуть исключение в случае возникновения ошибки.
        }

        Key getParent(const Key& key) const {
            if (auto it = tree.find(key); it != tree.end()) {
                return it->second.parent;
            } else {
                throw out_of_range(string("getParent( ) : not found : ") + to_string(key));
            }
        }

        void erase(const Key& key) {
            Key parent = getParent(key);
            for (const auto k : tree[key].children) {
                tree[k].parent = parent;
            }

            tree[parent].children.erase(key);
            tree.erase(key);
        }

        unordered_map<Key, Node> tree;
    };

    struct Game {
        void load( );
        void save( ) { }

        void update(double time, double /*dt*/) {
            PR;
            this->time = time;

            for (auto& kv : objects) {
                auto& object = kv.second;
                const auto& updaterTypes = object->updaterTypes;
                for (auto& updaterType : updaterTypes) {
                    auto it = updaters.find(updaterType);
                    if (it != updaters.end()) {
                        try {
                            it->second->update(object, *this);
                        } catch (const exception& ex) {
                            PM("WARN: exception : " + string(ex.what()));
                        }
                    } else {
                        PM("WARN: updaters.find() : false");
                    }
                }
            }

            for (auto& action : actions) {
                StatusList status = canUpdateAction(action);
                try {
                    updateAction(status);
                } catch (const exception& ex) {
                    PM("WARN: exception : " + string(ex.what()));
                }
            }
            actions.clear();

            for (auto& event : events) {
                updateEvent(event);
            }
            events.clear();
        }

        StatusList canUpdateAction(const ActionSPtr& action) const {
            PR;
            StatusList status;
            auto uidSrc = action->uidSrc;
            auto uidDst = action->uidDst;
            auto itObjectSrc = objects.find(uidSrc);
            auto itObjectDst = objects.find(uidDst);
            if (itObjectSrc != objects.end() && itObjectDst != objects.end()) {
                auto& objectSrc = itObjectSrc->second;
                auto& objectDst = itObjectDst->second;
                const auto& typesObject = objectDst->updaterActionTypes;
                for (const auto typeObject : typesObject) { // TODO optimization
                    auto it = updaterActions.find(typeObject);
                    if (it != updaterActions.end()) {
                        const auto& updaterAction = it->second;
                        auto statusNew = updaterAction->canUpdateAction(objectSrc, objectDst, *this, action);
                        status.append(statusNew);
                    } else {
                        PM("WARN: updaterActions.find() : false");
                    }
                }
                if (status.statusSuccessList.empty() && status.statusFailureList.empty()) {
                    PM("WARN: status is empty : " + string(action->type));
                }
            } else {
                if (itObjectSrc == objects.end()) {
                    PM("WARN: itObjectSrc not found: " + to_string((int) uidSrc));
                } else {
                    PM("WARN: itObjectDst not found: " + to_string((int) uidDst));
                }
            }
            return status;
        }

        void updateAction(StatusList& status) {
            PR;
            auto& statusSuccessList = status.statusSuccessList;
            auto& statusFailureList = status.statusFailureList;
            if (statusFailureList.empty()) {
                for (auto& [updaterAction, objectSrc, objectDst, action] : statusSuccessList) {
                    updaterAction->updateActionSuccess(objectSrc, objectDst, *this, action);
                }
            } else {
                for (auto& [updaterAction, objectSrc, objectDst, action] : statusFailureList) {
                    updaterAction->updateActionFailure(objectSrc, objectDst, *this, action);
                }
                for (auto& [updaterAction, objectSrc, objectDst, action] : statusSuccessList) {
                    updaterAction->updateActionFailure(objectSrc, objectDst, *this, action);
                }
            }
        }

        void updateEvent(const ActionSPtr& event) {
            PR;
            const auto updateEventLambda = [this](ObjectSPtr& object, const ActionSPtr& event, ActionType type) {
                const auto& updaterEventTypes = object->updaterEventTypes;
                const auto [first, second] = updaterEventTypes.equal_range(type);
                for (auto it = first; it != second; ++it) {
                    if (auto it2 = updaterEvents.find(it->second); it2 != updaterEvents.end()) {
                        try {
                            it2->second->updateEvent(object, *this, event);
                        } catch (const exception& ex) {
                            PM("WARN: exception : " + string(ex.what()));
                        }
                    } else {
                        PM("WARN: updaterEvents.find() : false");
                    }
                }
            };
            for (auto& [_, object] : objects) {
                {
                    const auto uid = object->properties.get<PropertyKey::ObjectInfo>()->uid;
                    PM("INFO: uid : " + to_string(uid));
                }
                updateEventLambda(object, event, event->type);
                updateEventLambda(object, event, ActionTypeN::Unknown);
                updateEventLambda(object, event, ActionTypeN::Any);
            }
        }

        void addObject(const ObjectSPtr& object, size_t objectParentUID) {
            PR;
            const auto& uid = object->properties.get<PropertyKey::ObjectInfo>()->uid;
            PM("(uid, parentUid): " + to_string(uid) + " " + to_string(objectParentUID));
            objects.insert({uid, object});
            tree.add(uid, objectParentUID);
            addEvent(make_shared<EventMessage>(UID::World, UID::World, "addObject(...)")); // XXX Test
        }

        void addUpdaterAction(const UpdaterActionSPtr& updaterAction) {
            PR;
            updaterActions.insert({updaterAction->type, updaterAction});
        }

        void addUpdaterEvent(const UpdaterEventSPtr& updaterEvent) {
            PR;
            updaterEvents.insert({updaterEvent->type, updaterEvent});
        }

        void addAction(const ActionSPtr& action) {
            PR;
            actions.push_back(action);
        }

        void addEvent(const ActionSPtr& action) {
            PR;
            events.push_back(action);
        }

        void addUpdater(const UpdaterSPtr& updater) {
            PR;
            updaters.insert({updater->type, updater});
        }

        list<ActionSPtr> events;
        list<ActionSPtr> actions;
        list<ActionSPtr> actionsActive; // TODO
        unordered_map<size_t, ObjectSPtr> objects;
        unordered_map<UpdaterType, UpdaterSPtr> updaters;
        unordered_map<UpdaterActionType, UpdaterActionSPtr> updaterActions;
        unordered_map<UpdaterActionType, UpdaterEventSPtr> updaterEvents;
        Tree<size_t> tree;

        size_t time;
    };

    class Session : public std::enable_shared_from_this<Session>, public Object {
        websocket::stream<tcp::socket> ws_;
        boost::asio::io_service::strand strand_;
        shared_ptr<Listener> listener;
        boost::beast::multi_buffer buffer_read;
        boost::beast::multi_buffer buffer_write;

    public:
        explicit Session(tcp::socket socket, shared_ptr<Listener> listener);

        void run() {
            PR;
            ws_.async_accept(
                    strand_.wrap(std::bind(&Session::on_accept,
                                           shared_from_this(),
                                           std::placeholders::_1)));
        }

        void on_accept(boost::system::error_code ec) {
            PR;
            if(ec) {
                PM("WARN : ec : " + ec.message());
                return;
            }

            do_read();
        }

        void do_read() {
            PR;
            ws_.async_read(
                    buffer_read,
                    strand_.wrap(std::bind(&Session::on_read,
                                           shared_from_this(),
                                           std::placeholders::_1)));
        }

        void on_read(boost::system::error_code ec);

        void on_write(boost::system::error_code ec) {
            PR;
            if(ec) {
                PM("WARN : ec : " + ec.message());
                return;
            }

            buffer_write.consume(buffer_write.size());
        }

        void do_add_action(ActionSPtr action) {
            PR;
            strand_.post(boost::bind(&Session::on_add_action, shared_from_this(), action));
        }

        void on_add_action(ActionSPtr action) {
            PR;
            boost::beast::ostream(buffer_write) << action;

            ws_.text(ws_.got_text());
            ws_.async_write(
                buffer_write.data(),
                strand_.wrap(std::bind(
                                 &Session::on_write,
                                 shared_from_this(),
                                 std::placeholders::_1)));
        }
    };

    class Listener : public std::enable_shared_from_this<Listener>, public Object {
        boost::asio::io_service::strand strand_;
        tcp::acceptor acceptor_;
        tcp::socket socket_;

    public:
        Listener(boost::asio::io_service& ios, tcp::endpoint endpoint)
            : strand_(ios) , acceptor_(ios) , socket_(ios) {
            PR;

            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = UID::Listener;
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            properties.set<PropertyKey::ListenerInfo>(make_shared<ListenerInfo>());

            updaterActionTypes = { };
            updaterTypes = { UpdaterType::Listener };

            boost::system::error_code ec;

            acceptor_.open(endpoint.protocol(), ec);
            if(ec) {
                PM("WARN : ec : " + ec.message());
                return;
            }

            acceptor_.bind(endpoint, ec);
            if(ec) {
                PM("WARN : ec : " + ec.message());
                return;
            }

            acceptor_.listen(boost::asio::socket_base::max_connections, ec);
            if(ec) {
                PM("WARN : ec : " + ec.message());
                return;
            }
        }

        void run() {
            PR;
            if(!acceptor_.is_open())
                return;

            do_accept();
        }

        void do_accept() {
            PR;
            acceptor_.async_accept(
                    socket_,
                    strand_.wrap(std::bind(&Listener::on_accept,
                                           shared_from_this(),
                                           std::placeholders::_1)));
        }

        void on_accept(boost::system::error_code ec) {
            PR;
            if(ec) {
                PM("WARN : ec : " + ec.message());
            } else {
                auto session = std::make_shared<Session>(std::move(socket_), shared_from_this());
                session->run();

                auto listenerInfo = properties.get<PropertyKey::ListenerInfo>();
                lock_guard<mutex> lck(listenerInfo->mtxIn);
                listenerInfo->actionsIn.push_back(make_shared<ActionAddObject>(UID::Listener, UID::World, session));
            }

            do_accept();
        }

        void do_add_action(shared_ptr<Session> session, ActionSPtr action) { // old
            PR;
            strand_.post(boost::bind(&Listener::on_add_action, shared_from_this(),
                                     session, action));
        }

        void on_add_action(shared_ptr<Session> /*session*/, ActionSPtr /*action*/) { // old
            PR;
            // session->actionsRead.push_back(action);
        }
    };

    Session::Session(tcp::socket socket, shared_ptr<Listener> listener)
        : ws_(std::move(socket)) , strand_(ws_.get_io_service()), listener(listener) {
        PR;

        auto objectInfo = make_shared<ObjectInfo>();
        objectInfo->uid = RandomUniform::next((size_t)UID::Last, size_t(-1));
        properties.set<PropertyKey::ObjectInfo>(objectInfo);

        properties.set<PropertyKey::SessionInfo>(make_shared<SessionInfo>());

        updaterActionTypes = { };
        updaterTypes = { UpdaterType::SessionInit };
        updaterEventTypes = { {ActionTypeN::Unknown, UpdaterActionType::EMessageGlobal} }; // TODO addEventType(...)
    }

    void Session::on_read(boost::system::error_code ec) {
        PR;

        if(ec == websocket::error::closed) {
            PM("session: close");
            // listener->do_close(shared_from_this()); // TODO
            return;
        }

        if(ec) {
            PM("WARN : ec : " + ec.message());
        } else {
            std::stringstream stream;
            stream << boost::beast::buffers(buffer_read.data());
            buffer_read.consume(buffer_read.size());

            ActionSPtr action;
            stream >> action;

            auto sessionInfo = properties.get<PropertyKey::SessionInfo>();
            lock_guard<mutex> lck(sessionInfo->mtxIn);
            sessionInfo->actionsIn.push_back(action);
        }

        do_read();
    }

    struct UpdaterSessionInit : Updater {
        UpdaterSessionInit( ) : Updater(UpdaterType::SessionInit) { }
        void update(ObjectSPtr& object, Game& game) const override {
            PR;

            game.addObject(make_shared<Player>(pair{50, 50}), UID::World); // TODO

            object->updaterTypes.erase(UpdaterType::SessionInit);
        }
    };

    struct Socket : public Object {
        Socket( ) {
            PR;
            auto objectInfo = make_shared<ObjectInfo>();
            objectInfo->uid = UID::Socket;
            properties.set<PropertyKey::ObjectInfo>(objectInfo);

            updaterActionTypes = { };
            updaterTypes = { UpdaterType::SocketInit, UpdaterType::Socket };
        }
    };

    struct UpdaterSocketInit : Updater {
        UpdaterSocketInit( ) : Updater(UpdaterType::SocketInit) { }
        void update(ObjectSPtr& object, Game& game) const override {
            PR;

            auto socketInfo = make_shared<SocketInfo>();
            socketInfo->address = "127.0.0.1";
            socketInfo->port = 12347;
            socketInfo->thread_count = 3;

            socketInfo->ios = make_shared<boost::asio::io_service>(socketInfo->thread_count);

            socketInfo->listener = make_shared<Listener>(*socketInfo->ios,
                    tcp::endpoint{boost::asio::ip::address::from_string(socketInfo->address), socketInfo->port});
            socketInfo->listener->run();
            game.addObject(socketInfo->listener, UID::Socket);

            socketInfo->threads.reserve(socketInfo->thread_count);
            for (size_t i = 0; i < socketInfo->thread_count; ++i) {
                socketInfo->threads.emplace_back([socketInfo] {
                        socketInfo->ios->run();
                });
            }

            object->properties.set<PropertyKey::SocketInfo>(socketInfo);

            object->updaterTypes.erase(UpdaterType::SocketInit);
        }
    };

    struct UpdaterSocket : Updater {
        UpdaterSocket( ) : Updater(UpdaterType::Socket) { }
        void update(ObjectSPtr& /*object*/, Game& game) const override {
            PR;
            game.addEvent(make_shared<EventMessage>(UID::World, UID::World, "UpdaterSocket(...)")); // XXX Test
        }
    };

    struct UpdaterListener : Updater {
        UpdaterListener( ) : Updater(UpdaterType::Listener) { }
        void update(ObjectSPtr& object, Game& game) const override {
            PR;
            auto listenerInfo = object->properties.get<PropertyKey::ListenerInfo>();
            if (listenerInfo->mtxIn.try_lock()) {
                for (auto action : listenerInfo->actionsIn) {
                    game.addAction(action);
                }
                listenerInfo->actionsIn.clear();
                listenerInfo->mtxIn.unlock();
            }
        }
    };

    struct UpdaterActionAddObject : UpdaterAction {
        UpdaterActionAddObject( ) : UpdaterAction(UpdaterActionType::AddObject, {ActionTypeN::AddObject}) { }

        StatusList canUpdateAction(const ObjectSPtr& objectSrc, const ObjectSPtr& objectDst,
                                           const Game&, const ActionSPtr& action) const override {
            StatusList status;
            if (ActionTypeN::AddObject == action->type) {
                status.statusSuccessList.push_back({shared_from_this(), objectSrc, objectDst, action});
            }
            return status;
        }

        void updateActionSuccess(ObjectSPtr&, ObjectSPtr&, Game& game, const ActionSPtr& action) const override {
            PR;
            if (ActionTypeN::AddObject == action->type) {
                auto actionAddObject = static_pointer_cast<ActionAddObject>(action);
                auto object = actionAddObject->object;
                game.addObject(object, UID::World); // XXX
            }
        }

        void updateActionFailure(ObjectSPtr&, ObjectSPtr&, Game&, const ActionSPtr&) const override {
            PR;
        }
    };

    struct UpdaterEventMessageGlobal : UpdaterEvent {
        UpdaterEventMessageGlobal( ) : UpdaterEvent(UpdaterActionType::EMessageGlobal, {ActionTypeN::Unknown}) { }

        void updateEvent(ObjectSPtr& object, Game&, const ActionSPtr& event) const override {
            PR;
            PM("INFO: type : " + string(event->type));
            if (ActionTypeN::EMessage == event->type) {
                auto sessionInfo = object->properties.get<PropertyKey::SessionInfo>();
                auto session = dynamic_pointer_cast<Session>(object);
                if (session) {
                    session->on_add_action(event);
                } else {
                    PM("WARN: dynamic_pointer_cast");
                }
            }
        }
    };

    struct UpdaterPlayer : Updater {
        UpdaterPlayer( ) : Updater(UpdaterType::Player) { }
        void update(ObjectSPtr& object, Game& game) const override {
            PR;
            auto objectInfo = object->properties.get<PropertyKey::ObjectInfo>();
            auto uid = objectInfo->uid;

            game.addAction(make_shared<EventMessage>(uid, uid, "UpdaterPlayer"));
        }
    };



    std::ostream& operator << (std::ostream& stream, const ActionSPtr& action) {
        Pipe::save(stream, action);
        return stream;
    }

    std::istream& operator >> (std::istream& stream, ActionSPtr& action) {
        Pipe::load(stream, action);
        return stream;
    }

    void Pipe::load(std::istream& stream, ActionSPtr& action) {
        PR;
        try {
            nlohmann::json json;
            stream >> json;
            const string& type = json.at(JN::type);
            auto it = pipeActions.find(type);
            if (it != pipeActions.end()) {
                it->second->load(json, action);
            } else {
                PM("WARN: unknown type : " + type);
            }
        } catch (const exception& ex) {
            PM("WARN: exception : " + string(ex.what()));
        }
        if (!action) {
            PM("WARN: action is nullptr");
            action = make_shared<ActionUnknown>();
        }
    }

    void Pipe::save(std::ostream& stream, const ActionSPtr& action) {
        PR;
        nlohmann::json json;
        try {
            auto it = pipeActions.find(action->type);
            if (it != pipeActions.end()) {
                it->second->save(json, action);
            } else {
                PM("WARN: unknown type : " + string(action->type));
            }
        } catch (const exception& ex) {
            PM("WARN: exception : " + string(ex.what()));
        }
        if (json.empty()) {
            PM("WARN: json is null");
            json = nlohmann::json::object();
        }
        stream << json;
    }

    void Game::load( ) {
        addObject(make_shared<World>(), UID::Root);

        addObject(make_shared<GameMng>(), UID::World);
        addObject(make_shared<Socket>(), UID::GameMng);

        addObject(make_shared<Gem>(pair{80, 50}), UID::World); // Test
        addObject(make_shared<Gem>(pair{50, 80}), UID::World); // Test
        addObject(make_shared<Gem>(pair{20, 50}), UID::World); // Test

        addUpdaterAction(make_shared<UpdaterActionAddObject>());

        addUpdaterEvent(make_shared<UpdaterEventMessageGlobal>());

        addUpdater(make_shared<UpdaterSocket>());
        addUpdater(make_shared<UpdaterSocketInit>());
        addUpdater(make_shared<UpdaterListener>());
        addUpdater(make_shared<UpdaterSessionInit>());
        addUpdater(make_shared<UpdaterPlayer>());
    }



    /*
      Structure:

      Objects:
      - Player
      - Bomb
      - Gem
      - Bot

      Player:
      - Properties:
        - LocationInfo:
          - position
          - size
          - direction
          - speed
        - experience
      - Updaters:
        - UpdateLocationInfo
      - UpdatersAction:
        - Destroy
        - ChangeDirection
      - Actions:
        - ChangeDirection
        - SetBomb

      Bomb:
      - LocationInfo
      - timer
      - radius

      Gem:
      - Properties:
        - LocationInfo
        - price

      Bot:
      - Properties:
        - LocationInfo
      - Updaters:
        - UpdateLocationInfo
      - UpdatersAction:
      - Actions:
    */


}



int main( ) {

    {
        PR;
        using namespace composite4;

        constexpr size_t timeMax = 1000;
        constexpr size_t dt = 1;

        Game game;
        game.load();
        for (size_t time = 0; time < timeMax; time += dt) {
            game.update(time, dt);
            std::this_thread::sleep_for(std::chrono::milliseconds(400));
        }
    }

    cout << endl << "  end " << endl;

    return 0;
}
