#ifndef __MCSCANNER_HPP__
#define __MCSCANNER_HPP__ 1

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include <iostream>
#include <sstream>
#include "mc_parser.tab.hh"

using namespace std;


namespace MC{

class MC_Scanner : public yyFlexLexer{
public:

   MC_Scanner(std::string *in) : yyFlexLexer(), yylval( nullptr )
   {
     istream *is = new stringstream(*in);
     switch_streams(is);
     cout << "scanner 1:" << endl;
     // loc = new MC::MC_Parser::location_type();
   };

   int yylex(MC::MC_Parser::semantic_type * const lval);
   // YY_DECL defined in mc_lexer.l
   // Method body created by flex in mc_lexer.yy.cc


private:
   /* yyval ptr */
   MC::MC_Parser::semantic_type *yylval;
   /* location ptr */
   // MC::MC_Parser::location_type *loc;
};

} /* end namespace MC */

#endif /* END __MCSCANNER_HPP__ */
