#include <cctype>
#include <fstream>
#include <cassert>

#include "mc_driver.hpp"

MC::MC_Driver::~MC_Driver()
{
   delete(scanner);
   scanner = nullptr;
   delete(parser);
   parser = nullptr;
}

bool MC::MC_Driver::parse( A *a, const char * const buf )
{
   delete(scanner);

   std::string str(buf);
   scanner = new MC::MC_Scanner(&str);

   delete(parser);
   parser = new MC::MC_Parser( (*scanner), a );

   const int accept( 0 );
   if( parser->parse() != accept )
   {
      return false;
   }
   return true;
}
