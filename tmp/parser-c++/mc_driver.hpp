#ifndef __MCDRIVER_HPP__
#define __MCDRIVER_HPP__ 1

#include <string>
#include <cstdint>
#include "mc_scanner.hpp"
#include "mc_parser.tab.hh"
#include "a.h"

namespace MC{

class MC_Driver{
public:
   MC_Driver() = default;

   virtual ~MC_Driver();

   bool parse( A *a, const char *str );

private:
   MC::MC_Parser  *parser  = nullptr;
   MC::MC_Scanner *scanner = nullptr;
};

} /* end namespace MC */
#endif /* END __MCDRIVER_HPP__ */
