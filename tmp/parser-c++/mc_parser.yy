%skeleton "lalr1.cc"
%debug
%defines
%define api.namespace {MC}
%define parser_class_name {MC_Parser}

%code requires{
#include "a.h"
   namespace MC {
      class MC_Driver;
      class MC_Scanner;
   }

// The following definitions is missing when %locations isn't used
# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

}

%parse-param { MC_Scanner  &scanner  }
%parse-param { A *a }

%code{
   #include <iostream>

   /* include for all driver functions */
   #include "mc_driver.hpp"
   #include "a.h"

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

%token               END    0     "end of file"
%token <std::string> WORD
%token <char>        CHAR


%%

list_option : END | list END;

list
  : item
  | list item
  ;

item
  : WORD           { a->i = 1; std::cout << "word : [" << $1 << "]" << std::endl; }
  | WORD CHAR WORD { a->i = 2; std::cout << "words : [" << $1 << "] [" << $3 << "]" << std::endl; }
  ;

%%


void
MC::MC_Parser::error( const std::string &err_message )
{
   std::cerr << "Error: " << err_message << "\n";
}
