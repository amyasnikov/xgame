%{
/* C++ string header, for string ops below */
#include <string>
#include <iostream>

using namespace std;

/* Implementation of yyFlexScanner */
#include "mc_scanner.hpp"
#undef  YY_DECL
#define YY_DECL int MC::MC_Scanner::yylex(MC::MC_Parser::semantic_type * const lval)

/* typedef to make the returns for the tokens shorter */
typedef MC::MC_Parser::token token;

/* define to keep from re-typing the same code over and over */
// #define STOKEN( x ) ( new std::string( x ) )

/* define yyterminate as this instead of NULL */
//#define yyterminate() return( token::END )

/* msvc2010 requires that we exclude this header file. */
//#define YY_NO_UNISTD_H

%}

%option debug
%option nodefault
%option yyclass="MC::MC_Scanner"
%option noyywrap
%option c++

%%
%{          // Code executed at the beginning of yylex
            yylval = lval;
%}

[\ ]        { cout << "space" << endl; }

[a-zA-Z]*   {
               yylval->build<std::string>( yytext );
               cout << "WORD: [" << yytext << "]"<< endl;
               return( token::WORD );
            }

.           {
               cout << "CHAR: [" << yytext << "]" << endl;
               yylval->build<char>( yytext[0]);
               return( token::CHAR );
            }
%%
