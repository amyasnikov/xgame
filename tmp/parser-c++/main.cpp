#include <iostream>
#include <cstdlib>

#include "mc_driver.hpp"
#include "a.h"

int
main( const int argc, const char **argv )
{
   std::cout << "Start\n";

   if(argc != 2 )
      return ( EXIT_FAILURE );
   MC::MC_Driver driver;

   std::cout << "Start\n";

   A a;
   bool isOk = driver.parse( &a, argv[1] );

   std::cout << "Results" << std::endl;
   std::cout << "isOk = " << std::boolalpha << isOk << std::endl;

   std::cout << "a.i = " << a.i << std::endl;

   return( EXIT_SUCCESS );
}
