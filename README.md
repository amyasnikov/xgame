# Simple 2D game. #



### creator: Alexander Myasnikov. ###

contact: myasnikov.alexander.s@gmail.com

XGAME version: 0.06



[git](https://gitlab.com/amyasnikov/xgame)

[git](https://bitbucket.org/myasnikov_alexander/xgame)

[issues](https://gitlab.com/amyasnikov/xgame/issues)

[wiki](https://gitlab.com/amyasnikov/xgame/wikis/home)


