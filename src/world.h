
#ifndef WORLD_H
#define WORLD_H



#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <cmath>
#include <functional>
#include "queue.h"
#include "logger.h"
#include "action.h"
#include "pos.h"



class Action;



class Tileset {
 public:
  Tileset(const std::string& name, const std::string& img, const int begin, const int end) : name(name), img(img), begin(begin), end(end) {
    DEBUG_GAME;
  }

  std::string getName( ) const {
    DEBUG_GAME;
    return name;
  }

  std::string getImg( ) const {
    DEBUG_GAME;
    return img;
  }

  int getBegin( ) const {
    DEBUG_GAME;
    return begin;
  }

  int getEnd( ) const {
    DEBUG_GAME;
    return end;
  }

 protected:
  std::string name;
  std::string img;
  int begin;
  int end;
};

class TileInfo {
 public:
  TileInfo(const int tileId, const bool availability) : tileId(tileId), availability(availability) {
    DEBUG_GAME;
  }

  bool isAvailability( ) const {
    return availability;
  }

 protected:
  int  tileId;
  bool availability;
};

class Object {
 public:
  Object(Queue* const queue, int id, const DPos& pos, const double width, const double height)
    : queue(queue), id(id), pos(pos), width(width), height(height) {
    DEBUG_GAME;
  }

  bool getAction(ActionPtr& action) {
    DEBUG_GAME;
    return queue->getAction(id, action);
  }

  bool getResult(ActionPtr& action) { // addResult()
    DEBUG_GAME;
    action->setResult(); // XXX
    return queue->addResult(id, action->getResult());
  }

  DPos getPos( ) {
    DEBUG_GAME;
    return pos;
  }

  int getId( ) {
    DEBUG_GAME;
    return id;
  }

  int getWidth( ) {
    DEBUG_GAME;
    return width;
  }

  int getHeight( ) {
    DEBUG_GAME;
    return height;
  }

  void setPos(const DPos& pos_) {
    DEBUG_GAME;
    pos = pos_;
  }

 protected:
  Queue* const queue;
  int  id;
  DPos pos;
  int  width;
  int  height;
};

class Tile {
 public:
  Tile() : pos(-1, -1) {
    DEBUG_GAME;
  }

  Tile(const IPos& pos) : pos(pos) {
    DEBUG_GAME;
  }

  Tile(const IPos& pos, const std::vector<int>& data) : pos(pos) , data(data) {
    DEBUG_GAME;
  }

  const std::vector<int>& getData( ) const {
    return data;
  }

  const IPos getPos() const {
    return pos;
  }

 protected:
  IPos                    pos;
  std::vector<int>        data;    // <tileNum>
  std::unordered_set<int> objects; // <idObject>
};



class World {
  friend class myTestSuite;
 public:
  World(Queue* const queue);
  ~World( );
  void load(const std::string& mapName);
  void update(const std::string& mapName);
  void start( );

  void changeState( );

 protected:
  Queue* const queue;

  int tileWidth;
  int tileHeight;
  int nextObjectId;
  int tileSpriteId;
  int offsetX;
  int offsetY;

  DPos spawn; // pos? object?
  std::vector<Tileset> tilesets;
  std::unordered_map<IPos, Tile> tiles;
  std::unordered_map<int /* tileId   */, TileInfo> tilesInfo;
  std::unordered_map<int /* objectId */, Object>   objects;


 public:

  bool executeToSpawn(Object* object) const {
    DEBUG_GAME;
    return moveTo(object, spawn);
  }

  bool moveTo(Object* object, const DPos& pos) const {
    DEBUG_GAME;
    object->setPos(pos);
    return true;
  }

  bool executeMove(Object* object, const double dx, const double dy) const {
    DEBUG_GAME;
    bool result = false;
    if (object) {
      DPos dpos(dx, dy);

      DPos posObject = object->getPos();
      DPos posObjectNew = posObject + dpos;
      IPos posTileNew;

      if (getTilePosIfExist(posObjectNew, posTileNew)) {
        if (isAvailabilityTile(posTileNew)) {
          object->setPos(posObjectNew);
          result = true;
        }
      }

    }
    return result;
  }

  bool executeGetPosition(Object* object, double& x, double& y) const {
    x = object->getPos().x();
    y = object->getPos().y();
    return true;
  }

  bool getPosition(Object* object, DPos& posObject) const {
    bool result = false;
    if (object) {
      posObject = object->getPos();
      result = true;
    }
    return result;
  }

  bool getMap(Object* object, Tile& tile) const {
    bool result = false;
    if (object) {
      IPos pos;
      if (getTilePosIfExist(object->getPos(), pos)) {
        if (getTile(pos, tile)) {
          result = true;
        }
      }
    }
    return result;
  }

  bool executeGetMap(Object* object, std::vector<Tile>& tiles) const {
    bool result = false;
    int range = 10;
    if (object) {
      IPos pos;
      if (getTilePosIfExist(object->getPos(), pos)) {
        int x = pos.x();
        int y = pos.y();
        Tile tile;
        for (int i = x - range; i <= x + range; i++) {
          for (int j = y - range; j <= y + range; j++) {
            IPos posLocal(i, j);
            if (getTile(posLocal, tile)) {
              tiles.push_back(tile);
            }
          }
        }
      }
      result = true;
    }
    return result;
  }

  bool executeGetTilesets(std::vector<Tileset>& tilesets_) const {
    tilesets_ = tilesets;
    return true;
  }



  // move to loadObject(json);
  void addObject(const int id, const double x, const double y, const double width, const double height) {
    DEBUG_GAME;
    objects.insert({id, Object(queue, id, DPos(x, y), width, height)});
  }

  void addObject(const double x, const double y) {
    DEBUG_GAME;
    nextObjectId++;
    addObject(nextObjectId, x, y, 0, 0);
  }

  void getTilePos(const DPos& posObject, IPos& posTile) const {
    DEBUG_GAME;
    posTile.x() = floor(posObject.x());
    posTile.y() = floor(posObject.y());
  }

  bool getTilePosIfExist(const DPos& posObject, IPos& posTile) const {
    DEBUG_GAME;
    bool result = false;
    posTile.x() = floor(posObject.x() / tileWidth);
    posTile.y() = floor(posObject.y() / tileHeight);
    if (isExistTail(posTile))
      result = true;
    return result;
  }



  bool isExistObject(const int id) const {
    DEBUG_GAME;
    return objects.count(id) > 0;
  }

  bool isExistTailInfo(const int tileId) const {
    DEBUG_GAME;
    return tilesInfo.count(tileId) > 0;
  }

  bool isExistTail(const IPos& pos) const {
    DEBUG_GAME;
    DEBUG_GAME_(" isExistTail size : %zd\n", tiles.size());
    DEBUG_GAME_(" isExistTail count : %zd\n", tiles.count(pos));
    return tiles.count(pos) > 0;
  }

  bool getTile(const IPos&pos, Tile& tile) const {
    bool result = false;
    if (isExistTail(pos)) {
      tile = tiles.at(pos);
      result = true;
    }
    return result;
  }

  bool isAvailabilityTile(const IPos& pos) const {
    bool result = true;
    Tile tile;
    if (getTile(pos, tile)) {
      const std::vector<int>& data = tile.getData();
      for (const int tileId : data) {
        if (isExistTailInfo(tileId)) {
          if (!tilesInfo.at(tileId).isAvailability()) {
            result = false;
            break;
          }
        }
      }
    }
    return result;
  }

};

#endif /* WORLD_H */
