
#ifndef PLAYER_H
#define PLAYER_H



#include <string>
#include <functional>
#include <memory>
#include "queue.h"



class Action;



class Player {
public:
  friend class myTestSuite;

  Player( Queue* const queue, const std::string &name );

  Action* getAction( ) const;
  std::string getName( ) const;
  void setX( double x_ ) { x = x_; }
  void setY( double y_ ) { y = y_; }
  double getX( ) { return x; }
  double getY( ) { return y; }

 protected:
  Queue* const queue;
  const std::string name;
  Action* action;
  double x, y; // position
};

#endif /* PLAYER_H */
