
#include "queue.h"
#include <string>
#include "logger.h"
#include "action.h"



Queue::Queue( ) : last_id(-1) {
  LOGGER_QUEUE;
}

Queue::~Queue( ) {
  LOGGER_QUEUE;
}

bool Queue::addAction(const int id, const int fd, ActionPtr& action) {
  LOGGER_QUEUE;
  LOG_QUEUE("id : %d", id);
  LOG_QUEUE("fd : %d", fd);

  std::lock_guard<std::mutex> lck(mtx);

  if (actionsNew.count(id) == 0) {
    actionsNew.insert({id, QueueAction(fd)});
  }

  QueueAction& queueAction = actionsNew.at(id);

  queueAction.fd        = fd; // XXX
  queueAction.last_time = 0;  // XXX
  queueAction.queueActionIn.push(action);

  LOG_QUEUE("size1: %zd", queueAction.queueActionIn.size());
  LOG_QUEUE("size2: %zd", queueAction.queueStrOut.size());
  LOG_QUEUE("size3: %zd", actionsNew.size());

  return true;
}

bool Queue::getAction(const int id, ActionPtr& action) {
  LOGGER_QUEUE;
  LOG_QUEUE("id : %d", id);

  std::lock_guard<std::mutex> lck(mtx);

  LOG_QUEUE("action = [%p]", (void *) action);

  bool result = false;
  if (actionsNew.count(id) != 0) {
    QueueAction& queueAction = actionsNew.at(id);
    if (!queueAction.queueActionIn.empty()) {
      action = queueAction.queueActionIn.front();
      queueAction.queueActionIn.pop();
      result = true;
    }
  }

  LOG_QUEUE("action = [%p]", (void *) action);

  return result;
}

bool Queue::addResult(const int id, const std::string& resultAction) {
  LOGGER_QUEUE;

  std::lock_guard<std::mutex> lck(mtx);

  bool result = false;
  if (actionsNew.count(id) != 0) {
    QueueAction& queueAction = actionsNew.at(id);
    queueAction.queueStrOut.push(resultAction);
    result = true;
  }

  return result;
}


bool Queue::addResult(const int id, ActionPtr& action) {
  LOGGER_QUEUE;

  bool result;

  if (action) {
    action->setResult();
    result = addResult(id, action->getResult());
  } else {
    result = false;
  }

  return result;
}

bool Queue::getResult(const int id, int &fd, std::string &resultAction) {
  LOGGER_QUEUE;

  std::lock_guard<std::mutex> lck(mtx);

  bool result = false;
  if (actionsNew.count(id) != 0) {
    QueueAction& queueAction = actionsNew.at(id);
    if (!queueAction.queueStrOut.empty()) {
      fd           = queueAction.fd;
      resultAction = queueAction.queueStrOut.front();
      queueAction.queueStrOut.pop();
      LOG_QUEUE("result: [%s]", resultAction.c_str());
      result = true;
    }
  }

  return result;
}

bool Queue::getNextId(int& id) {
  LOGGER_QUEUE;

  std::lock_guard<std::mutex> lck(mtx);

  bool result = false;
  if (!actionsNew.empty()) {
    std::unordered_map<int, QueueAction>::const_iterator iterator = actionsNew.find(last_id);
    if (iterator != actionsNew.cend()) {
      iterator++;
      if (iterator == actionsNew.cend())
        iterator = actionsNew.cbegin();
      id = iterator->first;
      last_id = id;
      result = true;
      LOG_QUEUE("id: %d", id);
    } else {
      last_id = actionsNew.cbegin()->first;
    }
  }

  return result;
}
