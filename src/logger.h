
#ifndef LOGGER_H
#define LOGGER_H



#include <cstdio>
#include <cstdarg>



#define LOGGER(type, indent)       Logger logger(indent, type, __FILE__, __PRETTY_FUNCTION__, __LINE__)
#define LOG(type, indent, ...)     Logger::log(type, indent, __LINE__, __VA_ARGS__)
#define ALOG(...)                  Logger::slog(__LINE__, __VA_ARGS__)



#define LOGGER_GAME                LOGGER("gam", Indents::game)
#define LOG_GAME(...)              LOG("gam", Indents::game, __VA_ARGS__)
#define LOGGER_MANAGER             // LOGGER("mng", Indents::manager)
#define LOG_MANAGER(...)           // LOG("mng", Indents::manager, __VA_ARGS__)
#define LOGGER_QUEUE               // LOGGER("que", Indents::queue)
#define LOG_QUEUE(...)             // LOG("que", Indents::queue, __VA_ARGS__)

#define LOGGER_OBJECT              LOGGER("obj", Indents::game)
#define LOG_OBJECT(...)            LOG("obj", Indents::game, __VA_ARGS__)
#define LOGGER_OTHER               LOGGER("oth", Indents::game)
#define LOG_OTHER(...)             LOG("oth", Indents::game, __VA_ARGS__)
#define LOGGER_WORLD               LOGGER("wrl", Indents::game)
#define LOG_WORLD(...)             LOG("wrl", Indents::game, __VA_ARGS__)



// legacy code:
#define DEBUG_GAME                 LOGGER_GAME
#define DEBUG_GAME_(...)           LOG_GAME(__VA_ARGS__)
#define DEBUG_MANAGER              LOGGER_MANAGER
#define DEBUG_MANAGER_(...)        LOG_MANAGER(__VA_ARGS__)
#define DEBUG_QUEUE                LOGGER_QUEUE
#define DEBUG_QUEUE_(...)          LOG_QUEUE(__VA_ARGS__)



class Logger {
 public:
  Logger(int& indent, const char* name, const char* file, const char* function, const int line)
  : indent(indent), name(name), file(file), function(function), line(line) {
    fprintf(stderr, "%s  %*sEntering %s : %d\n", name, indent, "", function, line);
    indent += 2;
  }

  ~Logger( ) {
    indent -= 2;
    fprintf(stderr, "%s  %*sLeaving %s\n", name, indent, "", function);
  }

  static void log(const char* name, int indent, int line, const char* format, ...)
  {
    fprintf(stderr, "%s  %*s:%5d >>   ", name, indent, "", line);

    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fprintf(stderr, "\n");
    fflush(stderr);
  }

  static void slog(int line, const char* format, ...)
  {
    fprintf(stderr, "     :%5d >>   ", line);

    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fprintf(stderr, "\n");
    fflush(stderr);
  }

 public:
  int&         indent;
  const  char* name;
  const  char* file;
  const  char* function;
  const  int   line;
};



class Indents {
 public:
  static int game;
  static int manager;
  static int queue;
};



#endif /* LOGGER_H */
