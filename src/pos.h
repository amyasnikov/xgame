
#ifndef POS_H
#define POS_H



template <typename Type>
class Pos : public std::pair<Type, Type>
{
 public:
  using std::pair<Type, Type>::pair;
  Type const& x( ) const { return std::pair<Type, Type>::first;  }
  Type const& y( ) const { return std::pair<Type, Type>::second; }
  Type&       x( )       { return std::pair<Type, Type>::first;  }
  Type&       y( )       { return std::pair<Type, Type>::second; }
  Pos operator+(Pos pos) { return Pos{x() + pos.x(), y() + pos.y()}; }
};

class IPos : public Pos<int> {
 public:
  using Pos::Pos;
};

class DPos : public Pos<double> {
 public:
  using Pos::Pos;
};

namespace std {
  template <>
  struct hash<IPos> {
    size_t operator()(const IPos& pos) const {
      return 14449 * hash<int>()(pos.x()) + hash<int>()(pos.y());
    }
  };
}



#endif /* POS_H */
