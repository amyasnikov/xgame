
#ifndef ACTION_H
#define ACTION_H

#include <string>
#include <unordered_map>
#include <memory>
#include "json.hpp"



class World;
class Object;
class Tileset;
class Tile;
class Action;



using ActionPtr = std::shared_ptr<Action>;



class TypeValue {
 public:
  static const constexpr char* requestId          = "100";
  static const constexpr char* sessionId          = "101";
  static const constexpr char* type               = "103";
  static const constexpr char* id                 = "104";
  static const constexpr char* request            = "105";
  static const constexpr char* response           = "106";
  static const constexpr char* login              = "107";
  static const constexpr char* password           = "108";
  static const constexpr char* dx                 = "109";
  static const constexpr char* dy                 = "110";
  static const constexpr char* x                  = "111";
  static const constexpr char* y                  = "112";
  static const constexpr char* error              = "113";
  static const constexpr char* name               = "name";
  static const constexpr char* img                = "img";
  static const constexpr char* begin              = "begin";
  static const constexpr char* end                = "end";
  static const constexpr char* data               = "data";
  static const constexpr char* next               = "119";
};



enum class TypeAction {
  unknown           = 1,
  nothing           = 2,
  getPosition       = 3,
  getTilesets       = 4,
  getMap            = 5,
  move              = 6,
  getSessionId      = 7,
  toSpawn           = 8,

  getMapXY          = 101,
  getIds,
};



class Action {
 public:
  Action();
  virtual ~Action( );

  virtual nlohmann::json setResponse( ) const = 0;

  virtual void execute ([[gnu::unused]] World* world) { };
  virtual void setParam([[gnu::unused]] const nlohmann::json& request) { };
  virtual void setError([[gnu::unused]] const std::string& error) { };

  void setHeader(const nlohmann::json& header);
  void setObject(Object* object_);
  void setResult( );

  TypeAction         getType( )      const { return type;      } // virtual ?
  int                getId( )        const { return id;        }
  int                getRequestId( ) const { return requestId; }
  const std::string& getSessionId( ) const { return sessionId; }
  const std::string& getResult( )    const { return result;    }

 protected:
  int                    requestId;
  std::string            sessionId; // XXX
  TypeAction             type;
  int                    id;

  std::string            result;

  Object* object;
};



class ActionNothing : public Action {
 public:
  virtual nlohmann::json setResponse( ) const { return {}; };
};



class ActionMove : public Action {
 public:
  virtual void execute(World* world);
  virtual void setParam(const nlohmann::json& request);
  virtual nlohmann::json setResponse( ) const;

 protected:
  double dx, dy;
};



class ActionGetPosition : public Action {
 public:
  virtual void execute(World* world);
  virtual nlohmann::json setResponse( ) const;

 protected:
  double x, y;
};



class ActionGetTilesets : public Action {
 public:
  virtual void execute(World* world);
  virtual nlohmann::json setResponse( ) const;
  /* setTileset(const tilesets); */

 protected:
  std::vector<Tileset> tilesets;
};



class ActionGetMap : public Action {
 public:
  virtual void execute(World* world);
  virtual nlohmann::json setResponse( ) const;

 protected:
  std::vector<Tile> tiles;
};



class ActionGetSessionId : public Action {
 public:
  /* virtual void execute( ); */
  virtual void setParam(const nlohmann::json& request);
  virtual nlohmann::json setResponse( ) const;
  const std::string& getLogin( ) const;
  const std::string& getPassword( ) const;
  void setSessionId(const std::string& sessionId_);

 protected:
  std::string login;
  std::string password;
  std::string newSessionId;
};



class ActionToSpawn : public Action {
 public:
  virtual void execute(World* world);
  virtual nlohmann::json setResponse( ) const;
};



class CreatorAction {
 public:
  CreatorAction(const std::string& actionString, ActionPtr& action);
  operator bool( );

 protected:
  ActionPtr& action;

  static std::unordered_map<TypeAction, Action* (*) ()> _actions; // const XXX
};

#endif /* ACTION_H */
