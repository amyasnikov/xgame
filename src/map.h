
#ifndef MAP_H
#define MAP_H



#include <map>
#include <memory>



/* class Cell; */
class Player;
class Direction;



class Map {
 public:
  friend class myTestSuite;

  Map( );
  ~Map( );

  void load( const std::string &name );
  void addPlayer( const std::string &playerName );

  void changePositionPlayer( const std::string &playerName, const std::shared_ptr<Direction> &direction );

 private:
  /* bool findPlayer( const std::string &playerName, Position &position ) const; */
  /* std::shared_ptr<Cell> getCell( const Position &position ) const; */
  /* void setCell( const Position &position, const std::shared_ptr<Cell> &cell_ ); */

 private:
  /* std::map<Position, std::shared_ptr<Cell>> map; */
  /* Position spawn; */
};

#endif /* MAP_H */
