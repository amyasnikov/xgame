
#include "world.h"
#include <thread>
#include <chrono>
#include <fstream>
#include "logger.h"
#include "player.h"
#include "action.h"
#include "json.hpp"



using json = nlohmann::json;

World::World(Queue* const queue) : queue(queue) {
  DEBUG_GAME;
}

World::~World( ) {
  DEBUG_GAME;
}

void World::load(const std::string& mapName) {
  DEBUG_GAME;
  DEBUG_GAME_("mapName = [%s]\n", mapName.c_str());

  std::ifstream ifmap("resource/" + mapName + "/map.json");
  std::string map;
  std::string tmp;

  while(!ifmap.eof()) {
    tmp.erase();
    ifmap >> tmp;
    map += tmp;
  }
  ifmap.close();

  try {
    const json map_json = json::parse(map);

    tileWidth                   = map_json.at("tilewidth");
    tileHeight                  = map_json.at("tileheight");
    nextObjectId                = map_json.at("nextobjectid");
    const int width             = map_json.at("width");
    const int height            = map_json.at("height");


    const json& properties_json = map_json.at("properties");
    tileSpriteId                = properties_json.at("tileSpriteId");
    const int voidTileId        = properties_json.at("voidTileId");
    const int spawnX            = properties_json.at("spawnX");
    const int spawnY            = properties_json.at("spawnY");
    const int offsetTileX       = properties_json.at("offsetTileX");
    const int offsetTileY       = properties_json.at("offsetTileY");
    offsetX                     = offsetTileX * tileWidth;
    offsetY                     = offsetTileY * tileHeight;
    spawn                       = DPos(spawnX + offsetX, spawnY + offsetY);


    const json& layers_json = map_json.at("layers");
    std::vector<std::vector<int>> layers(width * height);
    for (const json& layer_json : layers_json) {
      const bool visible = layer_json.at("visible");
      if (visible) {
        const std::string& type = layer_json.at("type");
        if (type == "tilelayer") {
          const std::vector<int>& data = layer_json.at("data");
          int ind = 0;
          for (const int val : data) {
            if (val > 0)
              layers[ind].push_back(val);
            ind++;
          }

        } else if (type == "objectgroup") {
          const json& objects_json = layer_json.at("objects");
          for (const json& object_json : objects_json) {
            const std::string& name = object_json.at("name");
            const int id            = object_json.at("id");
            const int width         = object_json.at("width");
            const int height        = object_json.at("height");
            const int x             = (int) object_json.at("x") + offsetX;
            const int y             = (int) object_json.at("y") + offsetY;
            addObject(id, x, y, width, height);
          }
        }
      }
    }
    int ind = 0;
    for (const std::vector<int>& data : layers){
      if (std::find(data.begin(), data.end(), voidTileId) == data.end()) {
        IPos posTile(ind % width + offsetTileX, ind / width + offsetTileY);
        tiles.insert({posTile, Tile(posTile, data)});
      }
      ind++;
    }


    const json& tilesets_json = map_json.at("tilesets");
    for (const json& tileset_json : tilesets_json) {
      const std::string& name = tileset_json.at("name");
      const int firstgid      = tileset_json.at("firstgid");
      const int tilecount     = tileset_json.at("tilecount");

      const json& properties_json = tileset_json.at("properties");
      const std::string& data     = properties_json.at("data");

      tilesets.push_back(Tileset(name, data, firstgid, firstgid + tilecount - 1));


      json::const_iterator tiles_it = tileset_json.find("tiles");
      if (tiles_it != tileset_json.end()) {
        const json& tiles_json = *tiles_it;
        for (json::const_iterator it = tiles_json.begin(); it != tiles_json.end(); ++it) {
          DEBUG_GAME_("tiles: key = %s\n", it.key().c_str());
          const json& objects_json = it.value().at("objectgroup").at("objects");
          for (const json& object_json : objects_json) {
            const std::string& name = object_json.at("name");
            // const int x             = (int) object_json.at("x") + offsetX;
            // const int y             = (int) object_json.at("y") + offsetY;

            // const int height        = object_json.at("height");
            // const int width         = object_json.at("width");
            // DEBUG_GAME_("object: R (%d, %d), (%d, %d)\n", x, y, x + width, y + height);

            json::const_iterator properties_it = object_json.find("properties");
            if (properties_it != object_json.end()) {
              for (json::const_iterator it = properties_it->begin(); it != properties_it->end(); ++it) {
                DEBUG_GAME_("tiles: key = %s\n", it.key().c_str()); // XXX
              }
            }

          }
        }
      }

    } // for tilesets_json



    // throw std::logic_error("stop load()");

  } catch (std::exception& ex) {
    DEBUG_GAME_("fail [%s]\n", ex.what());
    exit(EXIT_FAILURE);

  } catch (...) {
    DEBUG_GAME_("fail\n");
    exit(EXIT_FAILURE);
  }
}

void World::update(const std::string& mapName) {
  DEBUG_GAME;

  std::ifstream ifmap("resource/" + mapName + "/map.json");
  std::string   map;
  std::string   tmp;

  while(!ifmap.eof()) {
    tmp.erase();
    ifmap >> tmp;
    map += tmp;
  }
  ifmap.close();

  try {
    json map_json = json::parse(map);

    json& layers_json = map_json.at("layers");
    for (json& layer_json : layers_json) {
      const std::string& name = layer_json.at("name");
      if (name == "Objects") {
        layer_json["opacity"] = 1;
        json& objects_json = layer_json.at("objects");
        objects_json.clear();

        for (auto& it : objects) {
          Object& obj = it.second;

          json object_json;
          object_json["name"]   = "(empty)";
          object_json["id"]     = obj.getId();
          object_json["x"]      = obj.getPos().x() - offsetX;
          object_json["y"]      = obj.getPos().y() - offsetY;
          object_json["width"]  = obj.getWidth();
          object_json["height"] = obj.getHeight();

          objects_json.push_back(object_json);
        }

        break;
      }
    }



    std::ofstream ofmap("resource/" + mapName + "/map.json");
    ofmap << map_json.dump(2);
    ofmap.close();


  } catch (std::exception& ex) {
    DEBUG_GAME_("fail [%s]\n", ex.what());
    exit(EXIT_FAILURE);

  } catch (...) {
    DEBUG_GAME_("fail\n");
    exit(EXIT_FAILURE);
  }
}

void World::changeState( ) {
  DEBUG_GAME;

  ActionPtr action;
  for (auto& it : objects) {
    Object* object = &it.second;
    if (object->getAction(action)) {
      action->setObject(object);
      action->execute(this);
      // notify(object->getPos(), action.getResult()); // XXX
      object->getResult(action);
    }
  }

}

void World::start( ) {
  DEBUG_GAME;
  while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    changeState();
    // update("testMapTiles"); // XXX
  }
}
