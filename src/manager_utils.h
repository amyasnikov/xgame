
#ifndef MANAGER_UTILS_H
#define MANAGER_UTILS_H



#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>



enum WebSocketFrameType {
  ERROR_FRAME=0xFF00,
  INCOMPLETE_FRAME=0xFE00,

  OPENING_FRAME=0x3300,
  CLOSING_FRAME=0x3400,

  INCOMPLETE_TEXT_FRAME=0x01,
  INCOMPLETE_BINARY_FRAME=0x02,

  TEXT_FRAME=0x81,
  BINARY_FRAME=0x82,

  PING_FRAME=0x19,
  PONG_FRAME=0x1A
};



char *base64(const char *input, int length);

int createResponse(const char* request, int req_len, char* response, int resp_max_len);

bool makeFrame(enum WebSocketFrameType frame_type, unsigned char *msg, int msg_length, unsigned char *buffer, int buffer_size, int& len);

enum WebSocketFrameType getFrame(unsigned char* in_buffer, int in_length, unsigned char* out_buffer, int out_size, int* out_length);



#endif /* MANAGER_UTILS_H */
