
#include "game.h"
#include <thread>
#include "logger.h"
#include "world.h"
#include "queue.h"
#include "manager.h"



Game::Game( ) : queue(new Queue()), world(new World(queue)), manager(new Manager(queue)) {
  DEBUG_GAME;
}

Game::~Game( ) {
  DEBUG_GAME;
  delete queue;
  delete world;
  delete manager;
}

void Game::load(const std::string &mapName) {
  DEBUG_GAME;
  world->load(mapName);
}

void Game::start( ) {
  DEBUG_GAME;
  std::thread mngThread([=] { manager->start(); });
  mngThread.detach();
  world->start();
}

void Game::changeState( ) {
  DEBUG_GAME;
  world->changeState();
}
