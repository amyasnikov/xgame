
#include "manager_utils.h"



char* base64(const char *input, int length)
{
  BIO *bmem, *b64;
  BUF_MEM *bptr;

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char *buff = (char *) malloc(bptr->length); // XXX
  memcpy(buff, bptr->data, bptr->length-1);
  buff[bptr->length-1] = 0;

  BIO_free_all(b64);

  return buff;
}

int createResponse(const char* request, int req_len, char* response, int resp_max_len) {
  if (req_len < 14 || strncmp(request, "GET / HTTP/1.1", 14) != 0)
    return 0;

  char *key = strstr((char*)request, "Sec-WebSocket-Key: ");
  if (key == 0)
    return 0;

  key += 19;

  char key_ext[24 + 36];
  memcpy(key_ext, key, 24);
  memcpy(key_ext + 24, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", 36);

  char hash[20];
  SHA1((unsigned char *) key_ext, 24 + 36, (unsigned char *) hash);

  char *output = base64(hash, 20);

  char result[] = {
    "HTTP/1.1 101 Switching Protocols\r\n"
    "Upgrade: websocket\r\n"
    "Connection: Upgrade\r\n"
    "Sec-WebSocket-Accept: ____________________        \r\n\r\n"
    /* "Sec-WebSocket-Protocol: chat superchat\r\n\r\n" */
  };

  memcpy(strstr(result, "_____"), output, strlen(output));

  int len = strlen(result);

  if (len > resp_max_len) {
    return 0;
  }

  memcpy(response, result, len);

  return len;
}

bool makeFrame(enum WebSocketFrameType frame_type, unsigned char *msg, int msg_length, unsigned char *buffer, int buffer_size, int& len) {
  int pos = 0;
  int size = msg_length;
  buffer[pos++] = (unsigned char) frame_type; // text frame

  if(size <= 125) {
    buffer[pos++] = size;
  } else if(size <= 65535) {
    buffer[pos++] = 126; //16 bit length follows

    buffer[pos++] = (size >> 8) & 0xFF; // leftmost first
    buffer[pos++] = size & 0xFF;
  } else { // >2^16-1 (65535)
    buffer[pos++] = 127; //64 bit length follows

    // write 8 bytes length (significant first)

    // since msg_length is int it can be no longer than 4 bytes = 2^32-1
    // padd zeroes for the first 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = 0;
    }
    // write the actual 32bit msg_length in the next 4 bytes
    for(int i=3; i>=0; i--) {
      buffer[pos++] = ((size >> 8*i) & 0xFF);
    }
  }

  bool result = true;
  len = pos + size;

  if (pos + size > buffer_size) { // error
    result = false;
    size = buffer_size - pos;
    len = buffer_size;
  }

  memcpy((void*)(buffer+pos), msg, size);

  return result;
}

enum WebSocketFrameType getFrame(unsigned char* in_buffer, int in_length, unsigned char* out_buffer, int out_size, int* out_length) {
  //printf("getTextFrame()\n");
  if(in_length < 3)
    return INCOMPLETE_FRAME;

  unsigned char msg_opcode =  in_buffer[0] & 0x0F;
  unsigned char msg_fin    = (in_buffer[0] >> 7) & 0x01;
  unsigned char msg_masked = (in_buffer[1] >> 7) & 0x01;

  // *** message decoding

  int payload_length = 0;
  int pos = 2;
  int length_field = in_buffer[1] & (~0x80);
  unsigned int mask = 0;

  //printf("IN:"); for(int i=0; i<20; i++) printf("%02x ",buffer[i]); printf("\n");

  if(length_field <= 125) {
    payload_length = length_field;
  }
  else if(length_field == 126) { //msglen is 16bit!
    payload_length = in_buffer[2] + (in_buffer[3]<<8);
    pos += 2;
  }
  else if(length_field == 127) { //msglen is 64bit!
    payload_length = in_buffer[2] + (in_buffer[3]<<8);
    pos += 8;
  }
  //printf("PAYLOAD_LEN: %08x\n", payload_length);
  if(in_length < payload_length+pos) {
    return INCOMPLETE_FRAME;
  }

  if(msg_masked) {
    mask = *((unsigned int*)(in_buffer+pos));
    //printf("MASK: %08x\n", mask);
    pos += 4;

    // unmask data:
    unsigned char* c = in_buffer+pos;
    for(int i=0; i<payload_length; i++) {
      c[i] = c[i] ^ ((unsigned char*)(&mask))[i%4];
    }
  }

  if(payload_length > out_size) {
    //TODO: if output buffer is too small -- ERROR or resize(free and allocate bigger one) the buffer ?
  }

  memcpy((void*)out_buffer, (void*)(in_buffer+pos), payload_length);
  /* out_buffer[payload_length] = 0; */
  /* *out_length = payload_length+1; */
  *out_length = payload_length;

  //printf("TEXT: %s\n", out_buffer);

  if (msg_opcode == 0x0) return (msg_fin)?TEXT_FRAME:INCOMPLETE_TEXT_FRAME; // continuation frame ?
  if (msg_opcode == 0x1) return (msg_fin)?TEXT_FRAME:INCOMPLETE_TEXT_FRAME;
  if (msg_opcode == 0x2) return (msg_fin)?BINARY_FRAME:INCOMPLETE_BINARY_FRAME;
  if (msg_opcode == 0x8) return CLOSING_FRAME;
  if (msg_opcode == 0x9) return PING_FRAME;
  if (msg_opcode == 0xA) return PONG_FRAME;

  return ERROR_FRAME;
}
