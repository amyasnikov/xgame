
#ifndef MANAGER_H
#define MANAGER_H



#include <netinet/in.h>
#include <ev.h>
#include <algorithm>
#include <functional>
#include <set>
#include <string>
#include <map>
#include "logger.h"
#include "queue.h"
#include "action.h"
#include "manager_utils.h"



class Manager;
const int MAXLEN = 250 * 1024 * 1024;



class ClientInfo {
 public:
  ClientInfo(const std::string &login, const std::string &password) : login(login), password(password) {
    DEBUG_MANAGER;
    objectsId = {1, 5, 15};
  }

 /* private: */
  std::string login;
  std::string password;

  std::string   sessionId;
  std::set<int> objectsId;


  void setSessionId( ) {
    sessionId = hash(login);
  }

  static std::string hash(const std::string &str) {
    std::hash<std::string> str_hash;
    return std::to_string(str_hash(str)); // + rnd;
  }
};


class Client {
 public:
  Client(int fd) : fd(fd) {
    DEBUG_MANAGER;
  }

 /* private: */
  int fd;
  //time_t lastAction;
  char tmpBuffer[MAXLEN];
  char request  [MAXLEN];
  ClientInfo *clientInfo;
};



struct ev_io_extra {
  ev_io_extra(Manager* const manager, Queue* const queue, std::map<int, Client*> *clients)
      : manager(manager), queue(queue), clients(clients) { };

  struct ev_io io;
  Manager* const manager; // Перенести в ev_loop?
  Queue* const queue;
  std::map<int, Client*> *clients;
};

struct ev_idle_extra {
  ev_idle_extra(Queue* const queue, std::map<int, Client*> *clients, char *tmpBuffer)
      : queue(queue), clients(clients), tmpBuffer(tmpBuffer) { };

  struct ev_idle idle;
  Queue* const queue;
  std::map<int, Client*> *clients;
  char *tmpBuffer;
};



void accept_cb(struct ev_loop *loop, struct ev_io   *watcher, int revents);
void   read_cb(struct ev_loop *loop, struct ev_io   *watcher, int revents);
void   idle_cb(struct ev_loop *loop, struct ev_idle *watcher, int revents);



class Manager {
 public:
  Manager(Queue* queue) : queue(queue) {
    DEBUG_MANAGER;

    logins = {
      {"ivan", new ClientInfo("ivan", "ivan2")},
      {"anonimus", new ClientInfo("anonimus", "anonimus")}
    };
  }

  // valid sessionId
  bool validation(ActionPtr& action, Client *client) {
    DEBUG_MANAGER;
    bool result = false;
    if (action->getType() != TypeAction::getSessionId) {
      if (client->clientInfo) {
        std::string sessionId = action->getSessionId();
        if (client->clientInfo->sessionId == sessionId)
          result = true;
      }
    } else {
      result = true;
    }

    return result;
  }

  bool actionBefore(ActionPtr& action, Client* client) {
    DEBUG_MANAGER;
    bool result = false;
    if (action->getType() == TypeAction::getSessionId) {
      auto actionSession = std::static_pointer_cast<ActionGetSessionId>(action); // to execute()
      std::string login    = actionSession->getLogin();
      std::string password = actionSession->getPassword();
      if (logins.count(login) > 0) {
        ClientInfo *clientInfo = logins[login];
        if (clientInfo->login == login && clientInfo->password == password) {
          client->clientInfo = clientInfo;
          clientInfo->setSessionId();
          actionSession->setSessionId(clientInfo->sessionId);
          actionSession->setResult();
          result = true;
        }
      }
    } else if (action->getType() == TypeAction::getIds) {
      // TODO
    } else {
      if (client) {
        if (client->clientInfo) {
          std::set<int>& objectsId = client->clientInfo->objectsId;
          if (objectsId.count(action->getId())) {
            result = true;
          }
        }
      }
    }

    return result;
  }

  void start() {
    DEBUG_MANAGER;

    struct ev_loop *loop = ev_default_loop(0);

    int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    DEBUG_MANAGER_("sd = %d\n", sd);

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(2468);
    addr.sin_addr.s_addr = htons(INADDR_ANY);
    int b = bind(sd, (struct sockaddr *) &addr, sizeof(addr));
    DEBUG_MANAGER_("bind() = %d\n", b);
    if (b < 0)
      exit (EXIT_FAILURE);

    listen(sd, SOMAXCONN);



    struct ev_io_extra w_accept(this, queue, &clients);
    ev_io_init(&w_accept.io, accept_cb, sd, EV_READ);
    ev_io_start(loop, &w_accept.io);

    struct ev_idle_extra w_idle(queue, &clients, tmpBuffer);
    ev_idle_init(&w_idle.idle, idle_cb);
    ev_idle_start(loop, &w_idle.idle);



    while (true)
      ev_loop(loop, 0);
  }

 protected:
  Queue* const queue;
  std::map<int, Client*> clients; // <fd, client>
  char tmpBuffer[MAXLEN];

  std::map<std::string, ClientInfo*> logins; // <login, clientInfo>
};

#endif /* MANAGER_H */
