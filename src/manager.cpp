
#include "manager.h"
#include "game.h"
#include <thread>



void accept_cb(struct ev_loop *loop, struct ev_io *watcher, [[gnu::unused]] int revents) {
  DEBUG_MANAGER;

  struct ev_io_extra *watcher_extra = (ev_io_extra *) watcher;
  int client_sd = accept(watcher->fd, 0, 0);
  watcher_extra->clients->insert({client_sd, new Client(client_sd)});

  struct ev_io_extra *w_client = new ev_io_extra(watcher_extra->manager, watcher_extra->queue, watcher_extra->clients);
  ev_io_init(&w_client->io, read_cb, client_sd, EV_READ);
  ev_io_start(loop, &w_client->io);
}

void read_cb(struct ev_loop *loop, struct ev_io *watcher, [[gnu::unused]] int revents) {
  DEBUG_MANAGER;

  struct ev_io_extra *watcher_extra = (ev_io_extra *) watcher;
  Client *client = watcher_extra->clients->at(watcher->fd); // XXX

  ssize_t size = recv(watcher->fd, client->tmpBuffer, MAXLEN, MSG_NOSIGNAL);
  DEBUG_MANAGER_(" tmpBuffer: [%zd] [%.*s]\n", size, (int) size, client->tmpBuffer);
  if (size < 0)
    return;
  else if (size == 0) {
    client = nullptr;
    ev_io_stop(loop, watcher);
    delete watcher;
    return;
  } else {
    int size2 = createResponse(client->tmpBuffer, size, client->request, MAXLEN);

    if (size2 == 0) {
      enum WebSocketFrameType type = getFrame((unsigned char *) client->tmpBuffer, size, (unsigned char *) client->request, MAXLEN, &size2);
      DEBUG_MANAGER_(" request: [%d] [%.*s]\n", size2, size2, client->request);

      if (type == CLOSING_FRAME) {
        delete client;
        client = nullptr;
        ev_io_stop(loop, watcher);
        delete watcher;
        return;
      }

      // valid ?

      ActionPtr action;
      if (CreatorAction(std::string(client->request, size2), action)) {
        if (watcher_extra->manager->validation(action, client)) {
          watcher_extra->manager->actionBefore(action, client);
          watcher_extra->queue->addAction(action->getId(), watcher->fd, action);
          //watcher_extra->manager->actionAfter(action, client);
        }
      }

    } else {
      send(watcher->fd, client->request, size2, MSG_NOSIGNAL);
    }
  }
}

void idle_cb([[gnu::unused]] struct ev_loop *loop, struct ev_idle *watcher, [[gnu::unused]] int revents) {
  DEBUG_MANAGER;
  struct ev_idle_extra *watcher_extra = (ev_idle_extra *) watcher;
  auto queue = watcher_extra->queue;
  auto tmpBuffer = watcher_extra->tmpBuffer;

  int id;
  if (queue->getNextId(id)) {
    int fd;
    std::string resultAction;
    if (queue->getResult(id, fd, resultAction)) {
      int len;
      if (makeFrame(TEXT_FRAME, (unsigned char *) resultAction.c_str(), resultAction.size(), (unsigned char *) tmpBuffer, MAXLEN, len)) {
        DEBUG_MANAGER_(" tmpBuffer: [%d] [%.*s]\n", len, len, tmpBuffer);
        send(fd, tmpBuffer, len, MSG_NOSIGNAL);
      }
    }
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
}
