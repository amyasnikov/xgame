
#ifndef GAME_H
#define GAME_H



#include <string>



class World;
class Manager;
class Queue;



class Game {
 public:
  friend class myTestSuite;
  Game( );
  ~Game( );
  void load(const std::string &mapName);
  void start( );

 private:
  void changeState( );

  Queue   *queue;
  World   *world;
  Manager *manager;

};

#endif /* GAME_H */
