
#include "action.h"
#include "logger.h"
#include "world.h"
#include "json.hpp"



Action::Action( ) {
  DEBUG_GAME;
  object = nullptr;
}

Action::~Action( ) {
  DEBUG_GAME;
}

void Action::setHeader(const nlohmann::json& action_json) {
  DEBUG_GAME;
  try {
    id                            = action_json.at(TypeValue::id);
    requestId                     = action_json.at(TypeValue::requestId);
    sessionId                     = action_json.at(TypeValue::sessionId);
    const nlohmann::json& request = action_json.at(TypeValue::request);
    const int typeInt             = action_json.at(TypeValue::type);
    type                          = static_cast<TypeAction>(typeInt);
    setParam(request);
  } catch (...) {
  }
}

void Action::setObject(Object* object_) {
  DEBUG_GAME;
  object = object_;
}

void Action::setResult( ) {
  DEBUG_GAME;

  nlohmann::json action;

  if (id > 0)
    action[TypeValue::id] = id;

  action[TypeValue::type]      = static_cast<int>(type);
  action[TypeValue::requestId] = requestId;
  action[TypeValue::response]  = setResponse();

  result = action.dump();
  DEBUG_GAME_("result = [%s]\n", result.c_str());
}



void ActionMove::execute(World* world) {
  DEBUG_GAME;
  world->executeMove(object, dx, dy);
}

void ActionMove::setParam(const nlohmann::json& request) {
  DEBUG_GAME;
  try {
    dx = request.at(TypeValue::dx);
    dy = request.at(TypeValue::dy);
  } catch (...) {
  }
}

nlohmann::json ActionMove::setResponse( ) const {
  DEBUG_GAME;
  return {
    {TypeValue::dx, dx},
    {TypeValue::dy, dy}
  };
}



void ActionGetPosition::execute(World* world) {
  DEBUG_GAME;
  world->executeGetPosition(object, x, y);
}

nlohmann::json ActionGetPosition::setResponse( ) const {
  DEBUG_GAME;
  return {
    {TypeValue::x, x},
    {TypeValue::y, y}
  };
}



void ActionGetTilesets::execute(World* world) {
  DEBUG_GAME;
  world->executeGetTilesets(tilesets);
}

nlohmann::json ActionGetTilesets::setResponse( ) const {
  DEBUG_GAME;
  nlohmann::json response = {};
  for (const Tileset& tileset : tilesets) {
    response += {
      { TypeValue::name,  tileset.getName()  },
      { TypeValue::img,   tileset.getImg()   },
      { TypeValue::begin, tileset.getBegin() },
      { TypeValue::end,   tileset.getEnd()   }
    };
  }
  return response;
}



void ActionGetMap::execute(World* world) {
  DEBUG_GAME;
  world->executeGetMap(object, tiles);
}

nlohmann::json ActionGetMap::setResponse( ) const {
  DEBUG_GAME;
  nlohmann::json response = {};
  for (const Tile& tile : tiles) {
    response += {
      { TypeValue::x,    tile.getPos().x() },
      { TypeValue::y,    tile.getPos().y() },
      { TypeValue::data, tile.getData()    }
    };
  }
  return response;
}



// void ActionGetSessionId::execute( ) {
//   DEBUG_GAME;
// }

void ActionGetSessionId::setParam(const nlohmann::json& request) {
  DEBUG_GAME;
  try {
    login    = request.at(TypeValue::login);
    password = request.at(TypeValue::password);
  } catch (...) {
  }
}

nlohmann::json ActionGetSessionId::setResponse( ) const {
  DEBUG_GAME;
  return {
    {TypeValue::sessionId, newSessionId}
  };
}

const std::string& ActionGetSessionId::getLogin( ) const {
  DEBUG_GAME;
  return login;
}

const std::string& ActionGetSessionId::getPassword( ) const {
  DEBUG_GAME;
  return password;
}

void ActionGetSessionId::setSessionId(const std::string& sessionId_) {
  DEBUG_GAME;
  newSessionId = sessionId_;
}



void ActionToSpawn::execute(World* world) {
  DEBUG_GAME;
  world->executeToSpawn(object);
}

nlohmann::json ActionToSpawn::setResponse( ) const {
  DEBUG_GAME;
  return {};
}



CreatorAction::CreatorAction(const std::string& actionString, ActionPtr& action) : action(action) {
  DEBUG_GAME;

  action.reset();

  try {
    const nlohmann::json action_json = nlohmann::json::parse(actionString);

    DEBUG_GAME_("action_json.dump = [%s]\n", action_json.dump().c_str());

    const int        type_ = action_json.at(TypeValue::type);
    const TypeAction type  = static_cast<TypeAction>(type_);

    auto it = _actions.find(type);
    if (it != _actions.end()) {
      action.reset((it->second)());
      action->setHeader(action_json);
    }

  } catch (std::exception& ex) {
    DEBUG_GAME_("exception = [%s]\n", ex.what());
  }

}

CreatorAction::operator bool( ) {
  DEBUG_GAME;

  return (bool) action;
}

std::unordered_map<TypeAction, Action* (*) ()> CreatorAction::_actions = {
  {TypeAction::nothing,          []() -> Action* { return new ActionNothing();      }},
  {TypeAction::move,             []() -> Action* { return new ActionMove();         }},
  {TypeAction::getPosition,      []() -> Action* { return new ActionGetPosition();  }},
  {TypeAction::getSessionId,     []() -> Action* { return new ActionGetSessionId(); }},
  {TypeAction::getTilesets,      []() -> Action* { return new ActionGetTilesets();  }},
  {TypeAction::getMap,           []() -> Action* { return new ActionGetMap();       }},
  {TypeAction::toSpawn,          []() -> Action* { return new ActionToSpawn();      }},
};
