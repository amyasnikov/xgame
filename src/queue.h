
#ifndef QUEUE_H
#define QUEUE_H



#include <string>
#include <queue>
#include <unordered_map>
#include <mutex>
#include "action.h"



struct QueueAction {
  QueueAction(const int fd) : fd(fd), last_time(0) { }

  int                     fd;
  int                     last_time;
  std::queue<ActionPtr>   queueActionIn;
  std::queue<std::string> queueStrOut;
};



class Queue {
 public:
  Queue( );
  ~Queue( );

  bool addAction(const int id, const int fd, ActionPtr& action);
  bool getResult(const int id, int &fd, std::string &resultAction);

  bool getAction(const int id, ActionPtr& action);
  bool addResult(const int id, ActionPtr& action);
  bool addResult(const int id, const std::string& resultAction);

  bool getNextId(int& id);

 protected:
  std::mutex mtx;
  std::unordered_map<int, QueueAction> actionsNew; // <id_object, queueAction>
  int last_id;
};

#endif /* QUEUE_H */
