
CXXFLAGS = -std=gnu++17 -g -pedantic -Wall -Wextra -pthread -lev -lcrypto -I.
CXX = g++
SRCDIR = src
BINDIR = bin
BUILDDIR = build
FILES   = \
          pos \
          game \
          queue \
          world \
          logger \
          action \
          player \
          manager \
          manager_utils
SOURCES = $(patsubst %, $(SRCDIR)/%.cpp, $(FILES))
OBJECTS = $(patsubst %, $(BUILDDIR)/%.o, $(FILES))



all: dirs $(BINDIR)/server # $(BINDIR)/testCreateAction

dirs:
	mkdir -p $(BUILDDIR) $(BINDIR)

$(OBJECTS): $(BUILDDIR)/$(SOURCES)
	$(CXX)   -o $@   -c $(subst $(BUILDDIR), $(SRCDIR), $*).cpp   $(CXXFLAGS)

$(BINDIR)/server: $(OBJECTS)
	$(CXX)   -o $@   $(SRCDIR)/main.cpp $(OBJECTS)   $(CXXFLAGS)

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(BINDIR)

server:
	./$(BINDIR)/server



test:
	cxxtestgen --error-printer -o tests/runner.cpp tests/myTests.h
	$(CXX) -o $(BINDIR)/testrun -I$CXXTEST tests/runner.cpp $(OBJECTS) $(CXXFLAGS)
	./$(BINDIR)/testrun
