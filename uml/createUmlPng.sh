#!/bin/bash

for name in $*;
do
  echo $name
  java -jar plantuml.jar -o ../img -tsvg $name
  java -jar plantuml.jar -o ../img -tpng $name
done;
