
#include <cxxtest/TestSuite.h>
#include <memory>
#include "../src/logger.h"
#include "../src/game.h"
#include "../src/world.h"
#include "../src/map.h"
#include "../src/player.h"
#include "../src/action.h"
#include "../src/position.h"
#include "../src/direction.h"



class myTestSuite : public CxxTest::TestSuite {
public:

  void setUp( ) {
    /* Logger::isLogging = false; */
  }

  void tearDown( ) {
    /* Logger::isLogging = true; */
  }
  
  
  void testAdd( ) {
    TS_ASSERT_EQUALS( 1 + 1, 2 );
  }

  void testCanCreateWorld( ) {
    Game game;
    game.load( "testMap" );
    
    TS_ASSERT_DIFFERS( game.world, nullptr );
  }

  void testCanCreateMap( ) {
    Game game;
    game.load( "testMap" );
    
    TS_ASSERT_DIFFERS( game.world->map, nullptr );
  }

  void testCanMove( ) {
    Game game;
    game.load( "testMap" );
    game.createPlayer( "Vasya" );

    Position pos1;
    game.world->map->findPlayer( "Vasya", pos1 );
    TS_ASSERT_EQUALS( pos1.first, 7 );
    TS_ASSERT_EQUALS( pos1.second, 14 );

    game.world->players[0]->setAction( "MoveLeft" );
    game.changeState();

    Position pos2;
    game.world->map->findPlayer( "Vasya", pos2 );
    TS_ASSERT_EQUALS( pos2.first, 6 );
    TS_ASSERT_EQUALS( pos2.second, 14 );
  }

  
};
